# Builds the Docker image locally.

# Read full name of Docker image from separate text file.
source image.env
IMAGE=$REGISTRY/$GROUP/$PROJECT:$TAG

# Build image with that name from Docker spec file in this very folder.
docker build --tag $IMAGE .
