# Pushes the Docker image to the container registry.

# Read full name of Docker image from separate text file.
source image.env
IMAGE=$REGISTRY/$GROUP/$PROJECT:$TAG

# Push to container registry.
docker login $REGISTRY
docker push $IMAGE
docker logout $REGISTRY
