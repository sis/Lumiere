@rem Pushes the Docker image to the container registry.
@echo off

rem Don't pollute environment with our variables.
setlocal

rem Read full name of Docker image from separate text file.
for /f "tokens=*" %%i in ('type image.env') do set %%i
set IMAGE=%REGISTRY%/%GROUP%/%PROJECT%:%TAG%

rem Push to container registry.
docker login %REGISTRY%
docker push %IMAGE%
docker logout %REGISTRY%
