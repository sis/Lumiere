@rem Builds the Docker image locally.
@echo off

rem Don't pollute environment with our variables.
setlocal

rem Read full name of Docker image from separate text file.
for /f "tokens=*" %%i in ('type image.env') do set %%i
set IMAGE=%REGISTRY%/%GROUP%/%PROJECT%:%TAG%

rem Build image with that name from Docker spec file in this very folder.
docker build --tag %IMAGE% .
