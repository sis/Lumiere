﻿## GitLab CI configuration

The files in this folder are relevant to setting up continuous integration
(automated testing and releasing) in GitLab.

The main configuration file is `ci.yaml`. It would be `.gitlab-ci.yml`
by default, so GitLab needs to be made aware of its different name and
location. Go to Settings → CI/CD → "General pipelines" and enter the
relative path `.gitlab/ci.yaml` there.

The Docker container defined by `Dockerfile` has JavaFX and Apache Ant
pre-installed in order to be able to build the project and run the
test suite. The shell scripts (Linux, macOS) or batch scripts (Windows)
were used to build the image locally and upload it to this GitLab
instance's container registry.

The CI pipeline publishes the new version to the package registry once a
new Git tag is created. This can be done conveniently in the web UI. Just
note that the tag must correspond to the version string declared in
`version.xml`.
