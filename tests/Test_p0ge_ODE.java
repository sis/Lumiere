import lumiere.math.p0ge_ODE;
import lumiere.math.p0_ODE;

import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.ClassicalRungeKuttaIntegrator;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;
import org.apache.commons.math3.ode.nonstiff.DormandPrince54Integrator;
import org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;


@Test
public class Test_p0ge_ODE {

    public void hello() {
        System.out.println("Testing class: p0ge_ODE");
    }

    public static void integrate(){
        Double[] b = {2.0, 2.0};
        Double[] d = {0.5, 0.5};
        Double[] s = {0.5, 0.5};
        Double[] M = {0.0, 0.0};
        double   T = 10.;
        double delta = 1e-15;

        FirstOrderIntegrator integrator = new DormandPrince853Integrator(
            1e-4, 1., 1e-6, 1e-6
        );
        p0_ODE p_ode = new p0_ODE(
            b, new Double[]{1., 1.}, d, s, M, 2,
            new Double[]{1., 1.}, 1, new Double[]{0.}, false
        );
        p0ge_ODE pg_ode = new p0ge_ODE(
            b, new Double[]{1., 1.}, d, s, M, 2,
            1, T, new Double[]{0.}, p_ode, Integer.MAX_VALUE, false);
        pg_ode.p_integrator = integrator;

        double[] y0 = {1., 1., 1., 1.};
        double[] y  = new double[4];
        integrator.integrate(pg_ode, T, y0, 0., y);
        assertEquals(y[0], 0.13962038997202111, delta);
        assertEquals(y[1], 0.13962038997202111, delta);
        assertEquals(y[2], 5.347000517554212E-13, delta*y[2]);
        assertEquals(y[3], 5.347000517554212E-13, delta*y[3]);

        double[] p0 = {1., 1.};
        double[] p  = new double[2];
        integrator.integrate(p_ode,  T, p0, 0., p);
        assertEquals(p[0], 0.19098300619455585, delta);
        assertEquals(p[1], 0.19098300619455585, delta);

        double[] res1 = pg_ode.getP(8, false, new Double[]{0.});
        assertEquals(b[0], 2.0, delta);
        assertEquals(res1[0], 0.22344733651058374, delta);
        assertEquals(res1[1], 0.22344733651058374, delta);

        double[] res2 = pg_ode.getP(5, res1, 8, false, new Double[]{0.});
        assertEquals(res2[0], 0.19102382529867107, delta);
        assertEquals(res2[1], 0.19102382529867107, delta);

        res2 = pg_ode.getP(0, res1, 5, false, new Double[]{0.});
        assertEquals(res2[0], 0.19098347199434101, delta);
        assertEquals(res2[1], 0.19098347199434101, delta);
    }

    public static void integrators() {
        Double[] b = {1.1, 1.1};
        Double[] d = {1.0, 1.0};
        Double c1  = 0.01;
        Double c2  = 0.1;
        Double[] M = {b[0] - d[0] -c2/b[0], b[0] - d[0] - c2/b[0]};
        double psi = c2/c1 * M[0];
        Double[] s = {psi, psi};
        double T   = 1.;
        double[] y0 = {1.1, 1.1, 4., 9.};
        double[] y  = new double[4];
        FirstOrderIntegrator integrator;
        boolean benchmark = false;
        long   t0;
        double dt;
        double delta = 1e-15;

        p0_ODE p_ode = new p0_ODE(
            b, null, d, s, M, 2,
            new Double[]{1., 1.}, 1, new Double[]{0.}, true);
        p0ge_ODE pg_ode = new p0ge_ODE(
            b, null, d, s, M, 2, 1, T, new Double[]{0.},
            p_ode, Integer.MAX_VALUE, true);

        t0 = System.nanoTime();
        integrator = new ClassicalRungeKuttaIntegrator(0.01);
        integrator.integrate(pg_ode, T, y0, 0., y);
        dt = (System.nanoTime() - t0) / 1e6;
        if (benchmark)
            System.out.println(
                integrator.getClass().getSimpleName() + ": " +
                String.format("%.3f", dt) + " ms"
            );
        assertEquals(y[0],  1.013997181691256, delta);
        assertEquals(y[1],  1.013997181691256, delta);
        assertEquals(y[2],  4.544138299512013, delta);
        assertEquals(y[3], 10.224311173902032, delta);

        t0 = System.nanoTime();
        integrator = new DormandPrince853Integrator(1e-10, 1., 1e-100, 1e-10);
        integrator.integrate(pg_ode, T, y0, 0., y);
        dt = (System.nanoTime() - t0) / 1e6;
        if (benchmark)
            System.out.println(
                integrator.getClass().getSimpleName() + ": " +
                String.format("%.3f", dt) + " ms"
            );
        assertEquals(y[0],  1.013997181690731, delta);
        assertEquals(y[1],  1.013997181690731, delta);
        assertEquals(y[2],  4.544138299533722, delta);
        assertEquals(y[3], 10.224311173950872, delta);

        t0 = System.nanoTime();
        integrator = new DormandPrince853Integrator(1e-10, 1., 1e-100, 1e-7);
        integrator.integrate(pg_ode, T, y0, 0., y);
        dt = (System.nanoTime() - t0) / 1e6;
        if (benchmark)
            System.out.println(
                integrator.getClass().getSimpleName() + ": " +
                String.format("%.3f", dt) + " ms"
            );
        assertEquals(y[0],  1.013997181680725, delta);
        assertEquals(y[1],  1.013997181680725, delta);
        assertEquals(y[2],  4.544138300405868, delta);
        assertEquals(y[3], 10.224311175913202, delta);

        t0 = System.nanoTime();
        integrator = new DormandPrince54Integrator(1e-10, 1., 1e-100, 1e-7);
        integrator.integrate(pg_ode, T, y0, 0., y);
        dt = (System.nanoTime() - t0) / 1e6;
        if (benchmark)
            System.out.println(
                integrator.getClass().getSimpleName() + ": " +
                String.format("%.3f", dt) + " ms"
            );
        assertEquals(y[0],  1.013997181382942, delta);
        assertEquals(y[1],  1.013997181382942, delta);
        assertEquals(y[2],  4.544138259790356, delta);
        assertEquals(y[3], 10.224311084528303, delta);

        t0 = System.nanoTime();
        integrator = new HighamHall54Integrator(1e-10, 1., 1e-14, 1e-7);
        integrator.integrate(pg_ode, T, y0, 0., y);
        dt = (System.nanoTime() - t0) / 1e6;
        if (benchmark)
            System.out.println(
                integrator.getClass().getSimpleName() + ": " +
                String.format("%.3f", dt) + " ms"
            );
        assertEquals(y[0],  1.013997181123651, delta);
        assertEquals(y[1],  1.013997181123651, delta);
        assertEquals(y[2],  4.544138274875663, delta);
        assertEquals(y[3], 10.224311118470240, delta);

        t0 = System.nanoTime();
        integrator = new HighamHall54Integrator(1e-20, 1., 1e-320, 1e-10);
        integrator.integrate(pg_ode, T, y0, 0., y);
        dt = (System.nanoTime() - t0) / 1e6;
        if (benchmark)
            System.out.println(
                integrator.getClass().getSimpleName() + ": " +
                String.format("%.3f", dt) + " ms"
            );
        assertEquals(y[0],  1.013997181690194, delta);
        assertEquals(y[1],  1.013997181690194, delta);
        assertEquals(y[2],  4.544138299446740, delta);
        assertEquals(y[3], 10.224311173755160, delta);

    }

}
