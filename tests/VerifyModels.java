import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertEquals;

import beast.pkgmgmt.BEASTClassLoader;
import beast.base.parser.XMLParser;
import beast.base.inference.Runnable;
import beast.base.inference.Logger;
import beast.base.util.Randomizer;

import static com.google.common.io.Files.getNameWithoutExtension;
import static com.google.common.io.Files.readLines;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;
import java.nio.file.DirectoryStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import static java.lang.Math.abs;
import static java.lang.Math.max;


public class VerifyModels
{
    private static final PrintStream sysOut = System.out;
    private static final PrintStream sysErr = System.err;
    private static final String separator = FileSystems.getDefault().getSeparator();

    /** Redirects console output to an internal buffer. */
    private void redirectOutput() {
        System.setOut(new PrintStream(new ByteArrayOutputStream()));
        System.setErr(new PrintStream(new ByteArrayOutputStream()));
    }

    /** Restores output to the console. */
    private void restoreOutput() {
        System.setOut(sysOut);
        System.setErr(sysErr);
    }

    /** Returns if two floating-point numbers are "almost equal". */
    boolean is_close(double a, double b) {
        return abs(a-b) <= 1e-15 * max(abs(a), abs(b));
    }

    /** Verifies that solving the named model reproduces earlier results. */
    @Test(dataProvider="listModels")
    public void verifyModel(String name) throws Exception
    {
        System.out.println("Verifying model: " + name);
        assertFalse(name.endsWith(".xml"));

        // Make sure input file exists.
        Path root   = Paths.get(".");
        Path models = root.resolve("tests").resolve("models");
        assertTrue(Files.isDirectory(models));
        Path model = models.resolve(name + ".xml");
        assertTrue(Files.exists(model));

        // Make sure reference with expected output exists.
        Path references = root.resolve("tests").resolve("results");
        assertTrue(Files.isDirectory(references));
        Path ref = references.resolve(name + ".log");
        assertTrue(Files.exists(ref));

        // Log output to file with same name but in different folder.
        Path results = root.resolve("build").resolve("tests").resolve("results");
        if (!Files.isDirectory(results))
            Files.createDirectory(results);
        Path log = results.resolve(name + ".log");

        // Write state file to a temporary folder.
        Path state = Files.createTempFile(name, ".state");

        // Make sure all Beast "services" are registered.
        Path Lumiere = root.resolve("version.xml");
        Path Beast2  = root.resolve("lib/beast2/version.xml");
        assertTrue(Files.exists(Lumiere));
        assertTrue(Files.exists(Beast2));
        BEASTClassLoader.addServices(Lumiere.toString());
        BEASTClassLoader.addServices(Beast2.toString());

        // Solve the model.
        redirectOutput();
        try {
            XMLParser parser = new XMLParser();
            Runnable runner  = parser.parseFile(model.toFile(), false);
            Logger.FILE_MODE = Logger.LogFileMode.overwrite;
            System.setProperty("file.name.prefix", log.getParent() + separator);
            System.setProperty("state.file.name", state.toString());
            runner.setStateFile("", false);
            Randomizer.setSeed(1);
            runner.run();
        }
        catch (Exception exception) {
            restoreOutput();
            exception.printStackTrace();
            throw exception;
        }
        finally {
            restoreOutput();
        }

        // Read content of results log and reference file.
        assertTrue(Files.exists(log));
        List<String> log_lines = readLines(log.toFile(), StandardCharsets.UTF_8);
        List<String> ref_lines = readLines(ref.toFile(), StandardCharsets.UTF_8);
        assertFalse(log_lines.isEmpty());
        assertFalse(ref_lines.isEmpty());

        // Ignore anything but the last line.
        String log_line = log_lines.get(log_lines.size() - 1);
        String ref_line = ref_lines.get(ref_lines.size() - 1);
        assertFalse(log_line.isEmpty());
        assertFalse(ref_line.isEmpty());

        // Split tab-separated items.
        String[] log_parts = log_line.split("\t");
        String[] ref_parts = ref_line.split("\t");
        assertEquals(log_parts.length, ref_parts.length);

        // Compare numbers for equality within machine precision.
        for (int i = 0; i < log_parts.length; i++) {
            double log_value = Double.parseDouble(log_parts[i]);
            double ref_value = Double.parseDouble(ref_parts[i]);
            assertTrue(is_close(log_value, ref_value));
        }
    }

    /**
     * Provides the names of all model files inside the "models" folder.
     * <p>
     * Note: We would like to run these tests in parallel. That just means
     * adding a "parallel=true" argument to the DataProvider annotation.
     * But there seem to be some issues with that, only one of which is
     * redirecting stdout/stderr as we do above.
     */
    @DataProvider(name="listModels")
    public Object[][] listModels() throws IOException
    {
        // Find path to `models` folder from current working directory.
        Path root   = Paths.get(".");
        Path models = root.resolve("tests").resolve("models");
        assertTrue(Files.isDirectory(models));

        // Loop over all model files inside that folder.
        List<String> names = new ArrayList<>();
        try (DirectoryStream<Path> files = Files.newDirectoryStream(models, "*.xml")) {
            for (Path model : files) {
                String name = getNameWithoutExtension(model.toString());
                names.add(name);
            }
        }
        names.sort(Comparator.naturalOrder());

        // Return names in object array, as expected by TestNG.
        int n = names.size();
        Object[][] values = new Object[n][1];
        for (int i=0; i<n; i++)
            values[i][0] = names.get(i);
        return values;
    }

}
