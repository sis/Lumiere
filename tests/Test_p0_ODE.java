import lumiere.math.p0_ODE;

import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;


@Test
public class Test_p0_ODE {

    public void hello() {
        System.out.println("Testing class: p0_ODE");
    }

    public void integrate2d() {
        Double[] b = {1.03, 1.06};
        Double[] d = {1.00, 1.00};
        Double[] s = {0.02, 0.04};
        Double[] M = {3.00, 4.00};
        Double[] fitValues = {1.0, 1.0};

        FirstOrderIntegrator integrator = new DormandPrince853Integrator(
            1e-8, 100.0, 1e-20, 1e-9
        );
        FirstOrderDifferentialEquations ode = new p0_ODE(
            b, null, d, s, M, 2, fitValues, 1, new Double[]{0.}, false
        );

        double[] y0 = new double[]{1., 1.};
        double[] y  = new double[2];
        integrator.integrate(ode, 10, y0, 1, y);

        double delta = 1e-15;
        assertEquals(0.8765694996188548, y[0], delta);
        assertEquals(0.8765694996398216, y[1], delta);
    }

    public void integrate3d() {
        Double[] b = {1.03, 1.06, 1.5};
        Double[] d = {1.00, 1.00, 1.2};
        Double[] s = {0.02, 0.04, 0.1};
        Double[] M = {3., 1., 4., 1., 2., 2.};
        Double[] fitValues = {1., 1., 1.};

        FirstOrderIntegrator integrator = new DormandPrince853Integrator(
            1e-8, 100.0, 1e-10, 1e-10
        );
        FirstOrderDifferentialEquations ode = new p0_ODE(
            b, null, d, s, M, 3, fitValues, 1, new Double[]{0.}, false
        );

        double[] y0 = {1., 1., 1.};
        double[] y  = new double[3];
        integrator.integrate(ode, 10, y0, 1, y);

        double delta = 1e-15;
        assertEquals(0.8765694996080945, y[0], delta);
        assertEquals(0.8765694996495356, y[1], delta);
        assertEquals(0.8765694996219084, y[2], delta);
    }

}
