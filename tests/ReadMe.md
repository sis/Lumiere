﻿## Test suite

This folder contains the test suite for Lumière. Essentially, it runs
integration tests with models taken from the `examples` folder, copied
to the `models` folder of the test suite. The models are solved, and
the log output they produce is saved in `build/tests/results`. That
output is then compared to the one from an earlier run, provided as a
test fixture in `tests/results`.

We thus verify that the code keeps producing the same results. The
reference results could however be updated if legitimate modeling errors
are fixed in the library.
