import lumiere.math.SmallNumber;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

import java.lang.Math;


@Test
public class Test_SmallNumber {

    public void hello() {
        System.out.println("Testing class: SmallNumber");
    }

    SmallNumber[] S = {
            new SmallNumber(0),
            new SmallNumber(0),
            new SmallNumber(1.5),
            new SmallNumber(0),
            new SmallNumber(1., 400),
            new SmallNumber(1., -200),
            new SmallNumber(1., -1000)
    };

    public void init() {
        SmallNumber a;
        double delta = 1e-15;

        a = new SmallNumber();
        assertEquals(a.getMantissa(), 0.);
        assertEquals(a.getExponent(), 0);

        a = new SmallNumber(0., 0);
        assertEquals(a.getMantissa(), 0.);
        assertEquals(a.getExponent(), 0);

        a = new SmallNumber(0., 1);
        assertEquals(a.getMantissa(), 0.);
        assertEquals(a.getExponent(), 0);

        a = new SmallNumber(1., 1);
        assertEquals(a.getMantissa(), 1.);
        assertEquals(a.getExponent(), 1);

        a = new SmallNumber(0);
        assertEquals(a.getMantissa(), 0.);
        assertEquals(a.getExponent(), 0);

        a = new SmallNumber(10);
        assertEquals(a.getMantissa(), 1.25);
        assertEquals(a.getExponent(), 3);

        a = new SmallNumber(1e+50);
        assertEquals(a.getMantissa(), 1.0691058840368783, delta);
        assertEquals(a.getExponent(), +166);

        a = new SmallNumber(1e-50);
        assertEquals(a.getMantissa(), 1.8707220957835557, delta);
        assertEquals(a.getExponent(), -167);

        a = new SmallNumber(1e+100);
        assertEquals(a.getMantissa(), 1.142987391282275, delta);
        assertEquals(a.getExponent(), +332);

        a = new SmallNumber(1e-100);
        assertEquals(a.getMantissa(), 1.7498005798264096, delta);
        assertEquals(a.getExponent(), -333);

        a = new SmallNumber(1e+200);
        assertEquals(a.getMantissa(), 1.3064201766302603, delta);
        assertEquals(a.getExponent(), +664);

        a = new SmallNumber(1e-200);
        assertEquals(a.getMantissa(), 1.5309010345804195, delta);
        assertEquals(a.getExponent(), -665);

        a = new SmallNumber(1e+300);
        assertEquals(a.getMantissa(), 1.4932217896051503, delta);
        assertEquals(a.getExponent(), +996);

        a = new SmallNumber(1e-300);
        assertEquals(a.getMantissa(), 1.3393857589828342, delta);
        assertEquals(a.getExponent(), -997);
    }

    public void string() {
        SmallNumber a;

        a = new SmallNumber();
        assertEquals(a.toString(), "0E0");

        a = new SmallNumber(0);
        assertEquals(a.toString(), "0E0");

        a = new SmallNumber(2.0000000000249);
        assertEquals(a.toString(), "2.00000000002E0");

        a = new SmallNumber(2.0000000000250);
        assertEquals(a.toString(), "2.00000000002E0");

        a = new SmallNumber(2.0000000000251);
        assertEquals(a.toString(), "2.00000000003E0");
    }

    public void log() {
        SmallNumber a;
        a = new SmallNumber(Math.pow(Math.E, -50));
        assertEquals(a.log(), -50.0);
    }

    public void multiply() {
        SmallNumber a, b, c;

        a = new SmallNumber(0.24);
        b = new SmallNumber(9.62);
        c = SmallNumber.multiply(a, b);
        assertEquals(c.toString(), "2.3088E0");

        a = new SmallNumber(2E-200);
        b = new SmallNumber(2E-200);
        c = SmallNumber.multiply(a, b);
        assertEquals(c.toString(), "4E-400");
    }

    public void averageExponent() {
        assertEquals(SmallNumber.averageExponent(S), -114.28571428571429);
    }

    public void compareExponent() {
        assertEquals(SmallNumber.compareExponent(S), 1);
    }

}
