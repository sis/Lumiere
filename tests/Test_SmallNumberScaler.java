import lumiere.math.SmallNumberScaler;

import lumiere.math.SmallNumber;
import lumiere.math.ScaledNumbers;
import lumiere.math.p0ge_InitialConditions;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;


@Test
public class Test_SmallNumberScaler {

    double[] eqp = {0, 1, 0.5, 0.8, 0.9, 1.0, 0.6};
    SmallNumber[] eq = {
            new SmallNumber(0),
            new SmallNumber(0),
            new SmallNumber(1.5),
            new SmallNumber(0),
            new SmallNumber(1.0, +400),
            new SmallNumber(1.0, -200),
            new SmallNumber(1.0, -1000)
    };
    p0ge_InitialConditions conditions = new p0ge_InitialConditions(eqp, eq);

    public void hello() {
        System.out.println("Testing class: SmallNumberScaler");
    }

    public void scale() {
        double delta = 1e-15;
        ScaledNumbers scaled = SmallNumberScaler.scale(conditions);
        double[] equation = scaled.getEquation();
        assertEquals(equation[0], 0.0, delta);
        assertEquals(equation[1], 1.0, delta);
        assertEquals(equation[2], 0.5, delta);
        assertEquals(equation[3], 0.8, delta);
        assertEquals(equation[4], 0.9, delta);
        assertEquals(equation[5], 1.0, delta);
        assertEquals(equation[6], 0.6, delta);
    }

    public void scalingFactor() {
        ScaledNumbers scaled = SmallNumberScaler.scale(conditions);
        assertEquals(scaled.getScalingFactor(), 298);
    }

}
