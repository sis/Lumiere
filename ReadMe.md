﻿# Lumière

BEAST2 package for phylodynamics with adaptive molecular evolution
using the marginal fitness birth-death model.


## Building

Lumière depends on [Beast2] and [BeastFX], requires [Java 17 with
JavaFX] to run, and [Apache Ant] and [Git] to build. To compile the
source code and test it, simply run:
```
❯ ant
```

Find build artifacts and test reports inside the `build` folder.
Call `ant -p` to see individual automation options.

[Beast2]:  https://www.beast2.org
[BeastFX]: https://github.com/CompEvol/BeastFX
[Java 17 with JavaFX]: https://www.azul.com/downloads/?version=java-17-lts&package=jdk-fx
[Apache Ant]: https://ant.apache.org/bindownload.cgi
[Git]: https://git-scm.com


## Running

Use Ant to run the stand-alone `.jar` file and pass a Beast2 config
file as an argument, such as one of those in the `examples` folder:
```
❯ ant run -Dargs="examples/testMFBD_neutralFitModel_5sites.xml"
```

To use Lumière with Beauti, install the zip archive found in the `build`
folder ["by hand"] in your user's home directory.

To simulate phylogenetic trees, using Matlab, follow the directions in
`prototype/ReadMe.md`.

["by hand"]: http://www.beast2.org/managing-packages#Install_by_hand


## Developing

When developing, it is best to work with separate copies of the Beast2
and BeastFX dependencies. Clone them in the parent directory of this
folder here, so that they are stored side-by-side with Lumière.
```
❯ git clone https://github.com/CompEvol/beast2.git
❯ git clone https://github.com/CompEvol/BeastFX.git
```

When in doubt, refer to `build.xml` to find out which versions we use
for the build automation with Ant, which downloads its own copies of
Beast2 and BeastFX into the `lib` folder.

To configure the development environment in IntelliJ IDEA, open the
Project Structure dialog. On the Project page, make sure the SDK is
"zulu-17" and the language level "17".

On the Modules page, import Beast2 and BeastFX as modules, in addition
to Lumiere. Point the import wizard to the appropriate folders and
accept the defaults it suggests. (Though the "DensiTree" and "bundler"
libraries of Beast2 can be left out.) On the Dependencies tab for
Beast2, check the box next to the JUnit library and the `lib` folder to
"export" them, as BeastFX will need them as well. Then add Beast2 as a
module dependency of BeastFX, and both Beast2 and BeastFX as module
dependencies of Lumiere.

The Run/Debug configuration for Lumiere would start the main class
`beastfx.app.beast.BeastMain`. Add `-help` as a command-line argument to
see options. When passing an XML input file, you may have to specify
`-version_file version.xml`, so that BeastFX finds the "services" that
Lumière provides.

Note that we only need BeastFX and JavaFX in order to run Lumère as a
stand-alone application: via Ant, the `.jar` file, or the IDE. The code
itself does not depend on these front-end components. Neither does the
test suite, as it outputs everything to the console. Thus `ant test`
works without JavaFX installed.


## Credits

* David Rasmussen (original author)
* Marco Hamins-Puertolas (contributor)
* Maria Del (contributor)
* Cecilia Valenzuela Agüí (contributor)
* Timothy Vaughan (contributor)
* John Hennig (contributor)


## Change log

1.0.6 (2023–02–19)
* Verification: Results agree with Matlab prototype that underpinned the paper.
* Dependencies: Uses Ivy to manage external libraries (other than Beast2).
* Dependencies: No longer requires [MASTER] to build.
* Documentation: Local Javadoc site may be generated from the doc-comments.
* Testing: Extends code coverage and reduces run-time of test suite.

1.0.5 (2022–10–25)
* Removes dependency on [MultiTypeTree].
* Moves implementation of `ExcludablePrior` to `lumiere.util` name space.
* Refactoring changes to test suite.
* Fixes Beast2 XML export by Matlab prototype.
* Adds build automation.
* Adds this change log.

1.0.4 (2022–07–08)
* Removes `tiptypes`, `typeLabel`, `tipTypeArray` inputs from `FitnessBirthDeathModel`.
* Fixes some division-by-zero errors.
* Adds Beauti templates.

1.0.3 (2022–03–16)
* Moves code from `beast` to `lumiere` name space.
  <br>
  For example, `beast.evolution.speciation.FitnessBirthDeathModel` is now `lumiere.distribution.FitnessBirthDeathModel`.
* Uses `ExcludablePrior` from [MultiTypeTree] instead of Beast2.
* Adds test suite.
* Refactoring changes, removal of unused classes.

1.0.2 (2022–03–04)
* Fixes the broken constructor.

1.0.1 (2021–03–18)
* Fixes the pE vector.
* Reverts to previous code to cycle through migration matrix.
* A few other minor changes.
* Contributed by Marco Hamins–Puertolas.

1.0.0 (2019–08–26)
* [Code submitted] to publisher as supplement to [the paper].

[Code submitted]: https://github.com/elifesciences-publications/Lumiere
[the paper]:      http://dx.doi.org/10.7554/eLife.45562
[MultiTypeTree]:  https://github.com/tgvaughan/MultiTypeTree
[MASTER]:         https://github.com/tgvaughan/MASTER
