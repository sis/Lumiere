﻿## Verification

This folder contains a Matlab implementation of the MFBD model applied
to a simple, but non-trivial phylogenetic tree. The code is intended to
independently verify the correctness of the Beast extension Lumière, as
implemented in Java.

Run `verification.m` to compute the MFBD likelihood with Matlab and
export a Beast config file `verification.xml` with the same parameters
to be run with Lumière.

| Lumière | Matlab | paper | description |
| ------- | ------ | ----- | ----------- |
| `logP` | `logL` | equation (23) | full joint likelihood of sequence data and tree, $p(T, S_{1:L} \| \mu, \theta, F)$ |
| `full_tree_pD` | `full_tree_pD` | denominator of (22) | tree density, $p(T \| \mu, \theta, F)$ |
| `PrSN[k]` | `Ls` | (21) and numerator of (22) | density of the tree and sequence at site $k$, $p(T, S_k \| \mu, \theta, F)$ |
| `freq` | `initFreqs` | factor in (21) | initial frequencies of $M$ states at the root, $q(i)$ |
| `pSN.conditionsOnG[k][rootState]` | `root_pD(rootState,sites)` | factor in (21) and integral of (19) evaluated at the root | density of the tree and observed sequence data at site $k$ conditional on site $k$ being in state $i$ at the root, $p(T, S_k \| \mu, \theta, F, i) = D_{n,k,i}(t_\text{root})$ |
| `expectedP0` | `root_pE(states, sites)` | factor in (21) and integral of (15) evaluated at the root | probability that a lineage with expected fitness $u$ leaves no sampled descendants, approximates $E_{n,g}(t)$ of equation (9) for the fitness value $u$ closest to $f_g$ or $f_{n,k,i}$, $E_u(t_\text{root}) = E_{n,k,i}(t_\text{root})$ |
| `T` | ? |  | origin of the birth–death process |
| `orig` | ? |  | origin–root height |
| `pSN.conditionsOnG[siteCount][0]` | `fullLineProbsD{end}` |  |  |
| `pInitialConditions` | `pEMatrix` | | |
