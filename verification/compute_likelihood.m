function logL = compute_likelihood(params, phylo_tree, fitness_model)
% Computes likelihood of phylogentic tree under the marginal fitness
% birth-death model.

    % Set up.
    states = params.seqStates;         % states per site
    sites  = params.seqLength;         % sites in sequence
    fitClasses = length(fitness_model.fitPoints); % fit classes to track in fitSpace
    if ~isfield(params, 'startTime')
        params.startTime = 0.0;
    end
    times = params.startTime : params.dt : params.finalTime;
    totalTimes = length(times);
    timeIndexes = 1 : 1 : totalTimes;

    % Set return likelihood.
    logL = -Inf;

    % Set up fitness effects at each site.
    % (siteFitEffects should come from fitModel.)
    siteFitEffects = params.fitMtrx;

    % Set up gamma (mutation rate) matrix.
    upVec = (ones(states-1, 1) * params.sigma_up);
    downVec = (ones(states-1, 1) * params.sigma_down);
    params.gamma = diag(upVec, 1) + diag(downVec, -1);

    % Set up other parameters that should be invariant to sequence changes.
    params.nu = ones(states,1) * params.nu(1);
    params.samplingFraction = ones(states,1) * params.samplingFraction(1);
    params.rho = ones(states,1) * params.rho(1);

    % Get envScalers if using logistic growth model.
    if (params.logisticGrowth)
       envScalers = 1 - ...
                    (params.logisticK * params.initSize * ...
                     exp(params.logisticR * times) ...
                    ./ ...
                    (params.logisticK + ...
                     params.initSize * (exp(params.logisticR * times)-1)) ...
                    );
    else
       envScalers = ones(1, length(times));
    end

    % First solve probsE backwards in time for all integration times.
    % (t = 0 : params.dt : params.finalTime)
    pEMatrix = solve_probE_fitSpace();

    % For each external lineage, get tipState and solve siteLineProbsD.
    siteLineProbsD = cell(0);    % Holds marginal site prob densities, D_n,k,i.
    fullLineProbsD = cell(0);    % Holds whole lineage prob densities, D_n.
    for lin = 1 : phylo_tree.tipCount

        % Set site pDs according to sampled state.
        currLine = phylo_tree.nodes{lin};
        sample_pD = zeros(states,sites);
        tipSeq = currLine.lineSeqs(1,:) + 1;   % + 1 so indexed from 1
        if (isempty(currLine.lineTimes))
            % If loading an external phylogeny.
            lineStartTime = currLine.date;
        else
             % If lineTimes are stored from simulation.
            lineStartTime = currLine.lineTimes(1);
        end
        if (lineStartTime == params.finalTime)
            for s = 1 : sites
                % Sampling probability is rho at final time.
                sample_pD(tipSeq(s),s) = params.rho(tipSeq(s));
            end
            full_sample_pD = params.rho(1);
        else
            for s = 1 : sites
                % This should actually be d_i * s_i.
                sample_pD(tipSeq(s),s) = params.nu(tipSeq(s)) * ...
                                         params.samplingFraction(tipSeq(s));
            end
            full_sample_pD = params.nu(1) * params.samplingFraction(1);
        end

        % Track probabilites along lineages.
        genotypeProbs = get_genotypeProbs(sample_pD); % or norm_pD
        fitValue = dot(genotypeProbs, fitness_model.genotypeFitness);
        phylo_tree.nodes{lin}.pushLineEvent(fitValue, zeros(1,sites), lineStartTime);

        % Solve for pD along lineage.
        [siteLineProbsD{lin}, fullLineProbsD{lin}] = ...
            solve_siteLineProbsD(currLine, sample_pD, full_sample_pD);

    end

    % For each internal lineage update pD at coalescent event.
    for lin = phylo_tree.tipCount + 1 : phylo_tree.nodeCount

        currLine = phylo_tree.nodes{lin};

        if (length(currLine.children) > 1)

            if (isempty(currLine.lineTimes))
                currTime = currLine.date;
            else
                currTime = currLine.lineTimes(1);
            end
            pastIndexes = timeIndexes(times <= currTime);
            tx = pastIndexes(end); % next time index
            scaledBeta = params.beta_0 * envScalers(tx);

            % Get init pD for parent.
            child1_pD = siteLineProbsD{currLine.children(1)};
            child2_pD = siteLineProbsD{currLine.children(2)};

            % Get normalized site pD for both children.
            % Normalize by dividing each column by its sum.
            norm_child1_pD = child1_pD ./ repmat(sum(child1_pD), states, 1);
            norm_child2_pD = child2_pD ./ repmat(sum(child2_pD), states, 1);

            if (params.multiplicativeFitness)

                % fitness effects weighted by their probs
                weightedEffects_child1 = sum(norm_child1_pD .* siteFitEffects);

                % Take product over all other sites.
                expFitness = prod(weightedEffects_child1) ./ ...
                             weightedEffects_child1;
                marginalFitEffects_child1 = siteFitEffects .* ...
                                            repmat(expFitness, states, 1);

                % fitness effects weighted by their probs
                weightedEffects_child2 = sum(norm_child2_pD .* siteFitEffects);

                % Take product over all other sites.
                expFitness = prod(weightedEffects_child2) ./ ...
                             weightedEffects_child2;
                marginalFitEffects_child2 = siteFitEffects .* ...
                                            repmat(expFitness, states, 1);

                child1_full_Beta = scaledBeta * prod(weightedEffects_child1);
                child2_full_Beta = scaledBeta * prod(weightedEffects_child2);

            else

                marginalFitEffects_child1 = zeros(states,sites);
                marginalFitEffects_child2 = zeros(states,sites);
                for site = 1 : sites
                    for state = 1 : states
                       genotypeProbs = ...
                           get_condGenotypeProbs(norm_child1_pD, site, state);
                       marginalFitEffects_child1(state,site) = ...
                           dot(genotypeProbs, fitness_model.genotypeFitness);
                       genotypeProbs = ...
                           get_condGenotypeProbs(norm_child2_pD, site, state);
                       marginalFitEffects_child2(state,site) = ...
                           dot(genotypeProbs, fitness_model.genotypeFitness);
                    end
                end

                [genotypeProbs] = get_genotypeProbs(norm_child1_pD);
                child1_full_Beta = scaledBeta * ...
                                   dot(genotypeProbs, fitness_model.genotypeFitness);
                [genotypeProbs] = get_genotypeProbs(norm_child2_pD);
                child2_full_Beta = scaledBeta * ...
                                   dot(genotypeProbs, fitness_model.genotypeFitness);

            end

            parent_pD = zeros(states,1);
            for s = 1 : sites
                % Accounts for fitness effects at this site and the expected
                % effect at all other sites.
               child1_Beta = scaledBeta .* diag(marginalFitEffects_child1(:,s));
               child2_Beta = scaledBeta .* diag(marginalFitEffects_child2(:,s));
               parent_pD(:,s) = ( ...
                                    (child2_Beta * child1_pD(:,s)) .* ...
                                    child2_pD(:,s) ...
                                ) + ...
                                ( ...
                                    (child1_Beta * child2_pD(:,s)) .* ...
                                    child1_pD(:,s) ...
                                );
            end

            % Update full_pD.
            child1_full_pD = fullLineProbsD{currLine.children(1)};
            child2_full_pD = fullLineProbsD{currLine.children(2)};
            parent_full_pD = (child2_full_Beta * child1_full_pD * child2_full_pD) + ...
                             (child1_full_Beta * child2_full_pD * child1_full_pD);

            if (any(sum(isnan(parent_pD))) || any(sum(isinf(parent_pD))))
                disp('Found NaN or infinite updated pD');
                return
            end

            % Track probabilites along lineages.
            genotypeProbs = get_genotypeProbs(parent_pD); % or norm_pD
            fitValue = dot(genotypeProbs, fitness_model.genotypeFitness);
            phylo_tree.nodes{lin}.pushLineEvent(fitValue, zeros(1,sites), currTime);

        else

            parent_pD = siteLineProbsD{currLine.children(1)};
            parent_full_pD = fullLineProbsD{currLine.children(1)};

        end

        % Solve for pD along lineage.
        if (lin == phylo_tree.nodeCount)
            siteLineProbsD{lin} = parent_pD;     % This is the root!
            fullLineProbsD{lin} = parent_full_pD;
        else
            [siteLineProbsD{lin}, fullLineProbsD{lin}] = ...
                solve_siteLineProbsD(currLine, parent_pD, parent_full_pD);
        end

    end

    % Find root pE.
    if isempty(phylo_tree.nodes{end}.lineTimes)
        % Not sure what we should put here.
        rootTime = phylo_tree.nodes{end}.date;
    else
        rootTime = phylo_tree.nodes{end}.lineTimes(end);
    end
    pastIndexes = timeIndexes(times <= rootTime);
    tx = pastIndexes(end);        % next time index

    root_pD = siteLineProbsD{end};

    % Normalize by dividing each column by its sum.
    norm_pD = root_pD ./ repmat(sum(root_pD), states, 1);

    if (params.approxEProbs)

        % What if fitness effects are not multiplicative?

        % fitness effects weighted by their probs
        weightedEffects = sum(norm_pD .* siteFitEffects);

        % Take product over all other sites.
        expFitness = prod(weightedEffects) ./ weightedEffects;

        % Site fitness effects marginalized over all other sites.
        marginalFitEffects = siteFitEffects .* repmat(expFitness,states,1);

        % Compute site-specific pE values from fitness landscape.
        root_pE = zeros(states,sites);
        for site = 1 : sites
            for state = 1 : states
                fitValue = marginalFitEffects(state, site);
                eFitClass = fitness_model.getDiscreteFitPoint(fitValue);
                root_pE(state, site) = pEMatrix(eFitClass, tx);
            end
        end

        fitValue = prod(weightedEffects);
        eFitClass = fitness_model.getDiscreteFitPoint(fitValue);
        root_line_pE = pEMatrix(eFitClass, tx);

    else

        root_pE = zeros(states, sites);
        for site = 1 : sites
            for state = 1 : states
                genotypeProbs = get_condGenotypeProbs(norm_pD, site, state);
                root_pE(state, site) = dot(genotypeProbs, pEMatrix(:,tx));
            end
        end

        genotypeProbs = get_genotypeProbs(norm_pD);
        root_line_pE = dot(genotypeProbs, pEMatrix(:,tx));

    end

    % Need to get root_full_pD and root_full_pE.
    if (params.conditionOnSurvival)
        full_tree_pD = fullLineProbsD{end} / (1 - root_line_pE);
    else
        full_tree_pD = fullLineProbsD{end};
    end

    % Compute final likelihood.
    initFreqs = ones(states,1) / states;    % assuming a uniform prior
    logL = 0;
    for s = 1 : sites
        if (params.conditionOnSurvival)
            Ls = dot(initFreqs, root_pD(:,s) ./ (1 - root_pE(:,s)) );
            logL = logL + log(Ls);
        else
            Ls = dot(initFreqs, root_pD(:,s));
            logL = logL + log(Ls);
        end
    end

    % "Divide" by full prob of tree.
    logL = logL - ((sites-1) * log(full_tree_pD));


    function pEMatrix = solve_probE_fitSpace()

        % Set up params in fitness space.
        params.eNu = ones(fitClasses,1) * params.nu(1);
        params.eSamplingFraction = ones(fitClasses,1) * params.samplingFraction(1);
        params.eRho = ones(fitClasses,1) * params.rho(1);
        params.eBeta = params.beta_0 * diag(fitness_model.fitPoints);
        params.eGamma = fitness_model.getTransitionRates(params.sigma_up, ...
                            params.sigma_down, params.approxEProbs);

        % Set up pEMatrix to store E values.
        pEInit = ones(fitClasses,1) - params.eRho;
        pE = pEInit;
        pEMatrix = zeros(fitClasses,totalTimes);
        pEMatrix(:,totalTimes) = pE;

        for tx = totalTimes : -1 : 2

            scaledBeta = params.eBeta * envScalers(tx);

            dtx = times(tx) - times(tx-1);

            % death without sampling
            d_pE_1 = (1-params.eSamplingFraction) .* params.eNu;

            % no birth or migration
            d_pE_2 = ((scaledBeta + params.eGamma) * ones(fitClasses,1)) .* pE;

            % no death
            d_pE_3 = params.eNu .* pE;

            % birth of line in j -- both lineages produce no samples
            d_pE_4 = (scaledBeta * pE) .* pE; % simplified

            % migration into i from j
            d_pE_5 = params.eGamma * pE; % simplified

            d_pE = (d_pE_1 - d_pE_2 - d_pE_3 + d_pE_4 + d_pE_5) * dtx;
            pE = pE + d_pE;

            pEMatrix(:,tx-1) = pE;

        end

        if (sum(isnan(pE)) > 0 || sum(isinf(pE)) > 0)
            disp('Found negative or infinite pE at time zero');
            return
        end

    end


    function [pD, full_pD] = solve_siteLineProbsD(currLine, pD, full_pD)

        % Get start and end time for lineage
        if (isempty(currLine.lineTimes))
            currTime = currLine.date;
        else
            currTime = currLine.lineTimes(1);
        end
        endTime = currTime - currLine.parentDistance; % time of birth

        % Find next integration time and index tx
        pastIndexes = timeIndexes(times <= currTime);
        tx = pastIndexes(end); % next time index
        nextIntTime = times(tx); % next integration time

        while (nextIntTime > endTime)

            scaledBeta = params.beta_0 * envScalers(tx);

            % Compute normalized marginal site pDs (omega_n,k,i).
            % Normalize by dividing each column by its sum.
            norm_pD = pD ./ repmat(sum(pD),states,1);

            if (params.multiplicativeFitness)

                % Can compute marginalFitEffects for all sites at once
                % (if no epistasis).

                % fitness effects weighted by their probs
                weightedEffects = sum(norm_pD .* siteFitEffects);

                % Take product over all other sites
                expFitness = prod(weightedEffects) ./ weightedEffects;

                % site fitness effects marginalized over all other sites
                marginalFitEffects = siteFitEffects .* repmat(expFitness,states,1);

                % expected beta for entire lineage
                expBeta = scaledBeta * prod(weightedEffects);

            else

                % Compute marginalFitEffects based on conditional geno probs.
                marginalFitEffects = zeros(states,sites);
                for site = 1 : sites
                    for state = 1 : states
                        genotypeProbs = get_condGenotypeProbs(norm_pD, ...
                                                              site, state);
                        marginalFitEffects(state, site) = ...
                            dot(genotypeProbs, fitness_model.genotypeFitness);
                    end
                end
                genotypeProbs = get_genotypeProbs(norm_pD);
                expBeta = scaledBeta * dot(genotypeProbs, fitness_model.genotypeFitness);

            end

            % Get pE.
            pE = zeros(states,sites);
            if (params.approxEProbs)

                % Compute site-specific pE values based on current fitness.
                for site = 1 : sites
                    for state = 1 : states
                        fitValue = marginalFitEffects(state, site);
                        eFitClass = fitness_model.getDiscreteFitPoint(fitValue);
                        pE(state, site) = pEMatrix(eFitClass,tx);
                    end
                end

                % fitness effects weighted by their probs
                weightedEffects = sum(norm_pD .* siteFitEffects);

                % Find pE value for entire lineage.
                fitValue = prod(weightedEffects);
                eFitClass = fitness_model.getDiscreteFitPoint(fitValue);
                line_pE = pEMatrix(eFitClass, tx);

            else

                % Compute pE values based on conditional genotype probs.
                for site = 1 : sites
                    for state = 1 : states
                        genotypeProbs = get_condGenotypeProbs(norm_pD,site, state);
                        pE(state,site) = dot(genotypeProbs, pEMatrix(:,tx));
                    end
                end

                % For entire lineage.
                genotypeProbs = get_genotypeProbs(norm_pD);
                line_pE = dot(genotypeProbs, pEMatrix(:,tx));

            end

            % Track probabilites along lineages.
            genotypeProbs = get_genotypeProbs(norm_pD);
            fitValue = dot(genotypeProbs, fitness_model.genotypeFitness);
            phylo_tree.nodes{lin}.pushLineEvent(fitValue, zeros(1,sites), times(tx));

            % Update pD

            % no births or migration
            d_pD_1 = ((scaledBeta * marginalFitEffects) + ...
                      (params.gamma * ones(states,sites))) .* pD;

            % no deaths
            d_pD_2 = repmat(params.nu,1,sites) .* pD;

            % birth of line in j -- line in j produces no samples
            d_pD_3 = (scaledBeta * marginalFitEffects) .* pE .* pD;

            % birth of line in j -- line in j produces no samples
            % (Same as above?)
            d_pD_4 = (scaledBeta * marginalFitEffects) .* pD .* pE;

            % migration into i from j
            d_pD_5 = params.gamma * pD;    % without loops

            d_pD = -(d_pD_1 + d_pD_2) + d_pD_3 + d_pD_4 + d_pD_5;
            dt = times(tx) - times(tx-1);
            pD = pD + (d_pD * dt);

            % Update full_pD for entire lineage.
            d_full_pD_1 = (expBeta + params.nu(1)) * full_pD;
            d_full_pD_2 = 2 * expBeta * line_pE * full_pD; % Assumes pE's are all equal.
            d_full_pD = -d_full_pD_1 + d_full_pD_2;
            full_pD = full_pD + (d_full_pD * dt);

            if (any(sum(isnan(pD))) || any(sum(isinf(pD))))
                disp('Found NaN or infinite updated pD');
                return
            end

            tx = tx - 1;
            nextIntTime = times(tx);

        end

    end


    function genotypeProbs = get_genotypeProbs(norm_pD)
        genotypeProbs = zeros(fitness_model.genotypes, 1);
        for g = 1 : fitness_model.genotypes
            margSiteProbs = zeros(1,sites);
            for s = 1 : sites
                type = fitness_model.genoSeqArray(g,s) + 1;
                margSiteProbs(s) = norm_pD(type,s);
            end
            % Will these be normalized? No.
            genotypeProbs(g) = prod(margSiteProbs);
        end
        if (sum(genotypeProbs) <= 0)
            genotypeProbs = ones(fitness_model.genotypes, 1) / fitness_model.genotypes;
        else
            % Renormalize.
            genotypeProbs = genotypeProbs / sum(genotypeProbs);
        end
    end


    function genotypeProbs = get_condGenotypeProbs(norm_pD, site, state)
        % Compute genotype probs conditional on knowing state at one site.
        genotypeProbs = zeros(fitness_model.genotypes, 1);
        for g = 1 : fitness_model.genotypes
            if (fitness_model.genoSeqArray(g,site) + 1 ~= state)
                genotypeProbs(g) = 0;
            else
                margSiteProbs = zeros(1, sites);
                for s = 1 : sites
                    if s ~= site
                        type = fitness_model.genoSeqArray(g,s) + 1;
                        margSiteProbs(s) = norm_pD(type,s);
                    else
                        margSiteProbs(s) = 1;
                    end
                end
                % Will these be normalized? No.
                genotypeProbs(g) = prod(margSiteProbs);
            end
        end
        if (sum(genotypeProbs) <= 0)
            genotypeProbs = ones(fitness_model.genotypes,1) / fitness_model.genotypes;
        else
            % Renormalize.
            genotypeProbs = genotypeProbs / sum(genotypeProbs);
        end
    end


    function siteProb = get_margSiteProb(genotypeProbs, site, state)
        % Compute marginal site probabilities given joint genotype probabilites.
        siteProb = 0;
        for g = 1 : fitness_model.genotypes
            if (fitness_model.genoSeqArray(g,site) + 1 == state)
                siteProb = siteProb + genotypeProbs(g);
            end
        end
    end

end
