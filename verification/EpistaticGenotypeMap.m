classdef EpistaticGenotypeMap < handle
% Maps particular genotype sequences with fitness values.
% Allows for epistatis in double mutant.

    properties
        siteEffects     % Matrix representing fitness effects of each mutation
        sites           % Number of sites in seq
        seqArray
        fitVals
        genotypes
    end

    methods

        function obj = EpistaticGenotypeMap(fitMtrx, epiFitness)
        % Constructor method for new instance.

            obj.siteEffects = fitMtrx;
            [~, obj.sites] = size(fitMtrx);

            obj.genotypes = 4;

            % Get sequence for each genotype.
            obj.seqArray{1} = '11';
            obj.seqArray{2} = '10';
            obj.seqArray{3} = '01';
            obj.seqArray{4} = '00';

            % Get fitness for each genotype.
            obj.fitVals(1) = epiFitness;  % Fitness of double mutant can be arbitrary.
            obj.fitVals(2) = fitMtrx(2,1) * fitMtrx(1,2);
            obj.fitVals(3) = fitMtrx(1,1) * fitMtrx(2,2);
            obj.fitVals(4) = fitMtrx(1,1) * fitMtrx(1,2);

        end

        function state = getStateOfSeq(obj, seqStr)
            state = find(ismember(obj.seqArray, seqStr) == 1);
        end

        function updateGenotypeMap(obj, fitMtrx, epiFitness)
            obj.siteEffects = fitMtrx;
            % Get fitness for each genotype.
            % Fitness of double mutant can be arbitrary.
            obj.fitVals(1) = epiFitness;
            obj.fitVals(2) = fitMtrx(2,1) * fitMtrx(1,2);
            obj.fitVals(3) = fitMtrx(1,1) * fitMtrx(2,2);
            obj.fitVals(4) = fitMtrx(1,1) * fitMtrx(1,2);

        end

        function transRates = getTransitionRates(obj, sigma_up, sigma_down)
            % Hard-coded this for two-site model.
            types = length(obj.seqArray);
            transRates = zeros(types,types);

            % New transRate matrix
            transRates(1,2) = sigma_down;
            transRates(1,3) = sigma_down;
            transRates(2,1) = sigma_up;
            transRates(2,4) = sigma_down;
            transRates(3,1) = sigma_up;
            transRates(3,4) = sigma_down;
            transRates(4,2) = sigma_up;
            transRates(4,3) = sigma_up;
        end

    end
end
