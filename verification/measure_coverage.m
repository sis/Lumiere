% Measures code coverage by the verification procedure.
%
% Running this script will create a folder named `coverage`. Open the
% file `index.html` in that folder in a browser to see which lines of
% code are actually executed when running `verification.m`. In other
% words, it helps identify dead code that is not relevant for the
% calculation of the MFBD likelihood.

import matlab.unittest.plugins.CodeCoveragePlugin;
import matlab.unittest.plugins.codecoverage.CoverageReport;

runner = testrunner("minimal");
reportFormat = CoverageReport("coverage");
coverage = CodeCoveragePlugin.forFolder(".", "Producing", reportFormat);
runner.addPlugin(coverage);

tests = functiontests(localfunctions);
results = runner.run(tests);


function test_verification(testCase)
    verification();
end
