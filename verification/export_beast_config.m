function export_beast_config(beast_file, template_file, params, phylo_tree)
% Fills parameter values and tree structure into a Beast config file template.

    % Append .xml file suffix if missing. Take model name from file stem.
    if ~endsWith(beast_file, ".xml")
        model_name = beast_file;
        beast_file = model_name + ".xml";
    else
        parts = strsplit(beast_file, '.');
        model_name = strjoin(parts(1 : end-1));
    end

    % Read template from file.
    text = string(fileread(template_file, Encoding="UTF-8"));

    % Compose replacement string arrays for block elements.
    sequences = "";
    date_traits = "";
    type_traits = "";
    tip_names = phylo_tree.getTipNames();
    for n = 1 : length(tip_names)
        name  = string(tip_names{n});
        node  = phylo_tree.nodes{n};
        seqs  = node.lineSeqs(1,:);
        value = join(string(seqs), '');
        sequences(n) = append('<sequence id="seq_', name, '" taxon="', name, ...
                            '" totalcount="2" value="', value, '"/>');
        parts = strsplit(name, "_");
        date  = parts(end);
        date_traits(n) = name + "=" + date + ",";
        sampleLoc = node.lineStates(1);
        type_traits(n) = name + "=" + num2str(sampleLoc) + ",";
    end
    newick_tree = replace(phylo_tree.to_Newick(), ",", "," + newline).splitlines();

    % Substitute block elements.
    text = substitute(text, "{{alignment}}", sequences);
    text = substitute(text, "{{dateTrait}}", date_traits);
    text = substitute(text, "{{typeTrait}}", type_traits);
    text = substitute(text, "{{newick}}",    newick_tree);

    % Substitute inline elements.
    text = substitute(text, "{{dt}}", params.dt);
    text = substitute(text, "{{finalTime}}", params.finalTime);
    text = substitute(text, "{{approxEProbs}}", params.approxEProbs);
    text = substitute(text, "{{conditionOnSurvival}}", ...
                            params.conditionOnSurvival);

    text = substitute(text, "{{migrationMatrix}}", ...
                            params.sigma_up + " " + params.sigma_down);
    text = substitute(text, "{{mutFit}}", ...
                            params.fitMtrx(2,1) + " " + params.fitMtrx(2,2));
    text = substitute(text, "{{birthRate}}", params.beta_0);
    text = substitute(text, "{{deathRate}}", params.nu);
    text = substitute(text, "{{rhoSamplingProportion}}", params.rho);
    text = substitute(text, "{{samplingProportion}}", ...
                            num2str(params.samplingFraction));
    text = substitute(text, "{{tracelog}}", model_name + ".log");

    % Check that all placeholders have been replaced.
    placeholder = "{{" + optionalPattern(whitespacePattern) + ...
                alphanumericsPattern + ...
                optionalPattern(whitespacePattern) + "}}";
    if contains(text, placeholder)
        error("Template contains unknown placeholders.");
    end

    % Write output file, i.e. Beast input file.
    stream = fopen(beast_file, "wt", "n", "UTF-8");
    fprintf(stream, text);
    fclose(stream);

end
