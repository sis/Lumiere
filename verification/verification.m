function verification()
% Computes the MFBD log-likelihood of a tree with two samples and generates
% a corresponding Beast input file to be run with Lumière for comparison.

    % Set seed and select algorithm for random-number generation.
    % We will use the Mersenne Twister alogrithm (mt19937ar), the default in
    % Matlab. Lumière uses "MersenneTwisterFast" (MT19937) from `beast.util`,
    % which is very similar, if not the same. In any case, random numbers only
    % affect the tree "simulation" (i.e., generation), not the actual
    % likelihood computation.
    rng(1, "twister");

    % Set simulations control parameters.
    params.dt = 0.001;             % integration time step
    params.finalTime = 10.0;       % stop time
    params.minTreeSize = 2;        % minimum threshold size for simulated tree
    params.maxTreeSize = 2;        % maximum threshold size for simulated tree

    % Set options for computing BD likelihood.
    params.approxEProbs = true;        % Approximate E probs?
    params.multiplicativeFitness = true; % Multiplicative fitness effects may
                                       % mean faster calculation.
    params.conditionOnSurvival = true; % Condition likelihood on survival?
                                       % Should be true.
    params.coupleFitnessEffects = true; % Now assumed to always be true.

    % Mutation/fitness params
    params.initMutClass = 0;
    params.seqStates = 2;              % number of states at each site
    params.seqLength = 2;              % number of sites
    params.sd = 0.1;                   % selective cost of mutations:
                                       % fitness = (1-sd)^muts
    params.epiFitness = (1-params.sd)^2;  % fitness of double mutant with
                                          % possible epistasis
    params.sigma_up = 0.5;             % forward mutation rate
    params.sigma_down = 0.1;           % backward mutation rate

    % Fitness model
    params.fitMtrx = ones(2, params.seqLength);
    params.fitMtrx(2, 1 : params.seqLength) = 1 - params.sd;
    fitness_model = FitnessModel(params.fitMtrx, params.epiFitness, ...
                                 params.approxEProbs);

    % Demographic rates
    params.beta_0 = 0.25;              % base birth rate (λ₀ in paper)
    params.nu = 0.05;                  % death/removal rates (d in paper)

    % Environmental parameters
    params.logisticGrowth = false; % Rescale birth rates to get logistic growth?
    params.logisticK = 0.5;
    params.logisticR = 0.5;
    params.initSize = 0.005;

    % Parameters only used for tree simulations
    params.subPops = 1;                           % Should be states.
    params.noImmunity = ones(params.subPops,1);   % If true, then SIS.
                                                  % If false, then SIR.
    params.beta = diag(ones(params.subPops,1) * params.beta_0); % birth rates
    params.gamma = ones(params.subPops,params.subPops) * 0.0; % migration rates
    params.mu = zeros(params.subPops,1);               % host birth/death rates
    genoMap = EpistaticGenotypeMap(params.fitMtrx, params.epiFitness);
                                     % Still need this to simulate phylogenies.
    params.initPop = 1;
    params.popN = ones(params.subPops,1);

    % Sampling params
    params.samplingStartTimes = ones(params.subPops,1) * 0.01;
    params.samplingFraction = ones(params.subPops,1) * 0; % sampling in past
    params.rho = ones(params.subPops,1) * 0.1;            % sampling at present

    % Simulate a phylogenetic tree with only two samples.
    simFlag = 1;
    while(simFlag)

        [~, ~, ~, iList, iSeqs, simFlag] = simulate_sampling(params, genoMap);

        [sampleTimes, samplesAtTimes, sampleCount, ~] = ...
            get_serialSamples(params, iList, iSeqs);

        if sampleCount ~= 2
            disp("Tree contained " + num2str(sampleCount) + " samples.");
            simFlag = 1;
        end

    end
    iList(:,5) = iList(:,5) + 1;              % Shift mutLoad count.
    phylo_tree = PhyloTree(iList, iSeqs, sampleTimes, samplesAtTimes);
    phylo_tree.convertSeqsToStates(genoMap);

    % Compute approximate MFBD likelihood.
    logL = compute_likelihood(params, phylo_tree, fitness_model);
    disp("MFBD likelihood: log L = " + num2str(logL, '%.6f'));
    % Outputs: log L = -5.208171

    % Generate Beast input file by filling specifics into template.
    export_beast_config("verification.xml", "template.xml", params, phylo_tree);

end
