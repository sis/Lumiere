function text = substitute(text, placeholder, value)
% Substitutes a value for the placeholder in the text.
%
% The placeholder must be unique in the text. If the value is a
% single string, it will be substituted for the placeholder inline.
% If it is a string array, it will be inserted as a block of lines.
% The placeholder should then be on a separate line. Its
% indentation will be preserved throughout the block.

    % Coerce input arguments to string type.
    text = string(text);
    if length(text) ~= 1
        error("Text must be a single string.");
    end
    placeholder = string(placeholder);
    if length(placeholder) ~= 1
        error("Placeholder must be a single string.");
    end
    value = string(value);

    % Make sure placeholder exists and is unique.
    n = count(text, placeholder);
    if n == 0
        error("Placeholder """ + placeholder + """ not found in text.");
    end
    if n > 1
        error("Placeholder """ + placeholder + """ not unique in text.");
    end

    % Perform inline replacement with value.
    if length(value) == 1
        text = replace(text, placeholder, value);
        return;
    end

    % Determine indentation of block placeholder.
    [tokens, matches] = regexp(text, "^(\s*)" + placeholder + "\s*$", ...
                               "tokens", "match", ...
                               "lineanchors", "dotexceptnewline");
    if length(matches) ~= 1
        error("Placeholder """ + placeholder + """ not on separate line.");
    end
    indent = tokens{1};

    % Perform block replacement, preserving placeholder's indentation.
    text = replace(text, placeholder, join(value, newline + indent));

end
