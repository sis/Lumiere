classdef TreeNode < handle
% Represents a node in a phylogenetic tree.

    properties
        name             % node name
        height           % node age
        date             % node date (for timed trees)
        parent           % node parent
        parentDistance   % distance to parent node
        children         % array of node's children (can be more/less than two)
        state            % node state (if defined)
        stateSeq         % If we have sequence of states.
        annotationLabels % cell array of annotation labels
        annotationValues % cell array of annotation values
        save             % indicator var used for pruning trees and tracking
        sequence         % string containing sequence data

        % Added these to track sequence evolution through time.
        lineStates
        lineSeqs
        lineTimes

        % Added these to store/restore line state/seq data.
        storedLineSeqs;
    end

    methods

        function obj = TreeNode(name, height, date, children, state)
        % Constructor for nodes

            obj.name = name; %number of nodes
            obj.height = height;
            obj.date = date;
            obj.children = children;
            obj.state = state;
            obj.stateSeq = 0;
            obj.annotationLabels = cell(0);
            obj.annotationValues = cell(0);
            obj.save = false; % used for pruning trees
            obj.sequence = '';

            % Added these to track sequence evolution through time.
            obj.lineStates = [];
            obj.lineSeqs = [];
            obj.lineTimes = [];
        end

        function setParent(obj, parent, parentDistance)
            obj.parent = parent;
            obj.parentDistance = parentDistance;
        end

        function pushLineEvent(obj, state, seq, time)
            entry = length(obj.lineStates) + 1;
            obj.lineStates(entry) = state;
            obj.lineSeqs(entry,:) = seq;
            obj.lineTimes(entry) = time;
        end

        function storeLineData(obj)
            obj.storedLineSeqs = obj.lineSeqs;
        end

        function addSequence(obj, seq)
            %obj.sequence = seq;  % Changed this to map ebola seqs to ephylo.
            obj.lineSeqs = seq;
        end

        function addStateSeq(obj, seq)
            obj.stateSeq = seq;
        end

        function addAnnotation(obj, label, val)
            obj.annotationLabels{end+1} = label;
            if (~isempty(val))
                obj.annotationValues{end+1} = strtrim(val);
            else
                obj.annotationValues{end+1} = 'NA';
            end
        end

        function markAsSaved(obj)
            obj.save = true;
        end

    end
end
