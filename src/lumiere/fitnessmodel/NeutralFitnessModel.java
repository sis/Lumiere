package lumiere.fitnessmodel;

import beast.base.core.Description;


/**
 * Multi-site fitness model where all sites are neutral and have no
 * effects on fitness.
 *
 * @author David Rasmussen
 */

@Description(
"Multi-site fitness model where all sites are neutral and have no "
+ "effects on fitness.")

public class NeutralFitnessModel extends MultiSiteFitnessModel {

    @Override
    public void setUp(Integer[] states, int sites) {

        int maxState = states[0]; // assumes all sites have same # of states
        seqStates = states;
        seqSites = sites;
        siteFitnessEffects = new double[seqSites][maxState];

        for (int k=0; k<seqSites; k++) {
            for (int i=0; i<maxState; i++) {
                // All sites are neutral and have fitness = 1.0 under this model.
                siteFitnessEffects[k][i] = 1.0;
            }
        }

        // Set up fitness space -- only one state with fitness 1.0
        fitSpaceStates = 1;
        fitSpaceValues = new Double[1];
        fitSpaceValues[0] = 1.0;
        fitSpaceGammas = new Double[1];
        fitSpaceGammas[0] = 0.0; // no transitions
    }

    /** No-op. Nothing to update. */
    @Override
    public void update() {}

    /** Always zero for this model. */
    @Override
    public int getClosestFitSpaceState(double fit) {
        return 0;
    }

}
