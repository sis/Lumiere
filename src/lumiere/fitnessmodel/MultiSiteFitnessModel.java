package lumiere.fitnessmodel;

import lumiere.math.SmallNumber;

import beast.base.inference.CalculationNode;
import beast.base.core.Description;
import beast.base.core.Loggable;

import java.io.PrintStream;


/**
 * Multi-site fitness model.
 * <p>
 * This is the abstract class for all multi-site models.
 *
 * @author David Rasmussen
 */

@Description(
"Multi-site fitness model. "
+ "This is the abstract class for all multi-site models.")

public abstract class MultiSiteFitnessModel
    extends CalculationNode
    implements Loggable
{

    /** ... */
    protected Integer[] seqStates;

    /** ... */
    protected int seqSites;

    /** ... */
    protected int fitSpaceStates;

    /** ... */
    protected Double[] fitSpaceValues;

    /** ... */
    protected Double[] fitSpaceGammas;

    /** ... */
    protected double[][] siteFitnessEffects;

    /** ... */
    protected int[][] genotypes;

    /** Initializes an instance of this class. */
    @Override
    public void initAndValidate() {}

    /**
     * ...
     *
     * @param states the sequence of states
     * @param sites the number of sites in the sequence alignment
     */
    public void setUp(Integer[] states, int sites) {
        seqStates = states;
        seqSites = sites;
        siteFitnessEffects = new double[seqSites][];
        for (int k=0; k<seqSites; k++)
            siteFitnessEffects[k] = new double[states[k]];
    }

    /** ... */
    public abstract void update();

    /** Returns discrete point in fitness space closest to fitness
        value <code>fit</code>. */
    public abstract int getClosestFitSpaceState(double fit);

    /** Returns <code>siteFitnessEffects</code>. */
    public double[][] getSiteFitnessEffects() {
        return siteFitnessEffects;
    }

    /** Returns <code>fitSpaceStates</code>. */
    public int getFitSpaceStates() {
        return fitSpaceStates;
    }

    /** Returns <code>fitSpaceValues</code>. */
    public Double[] getFitnessSpaceValues() {
        return fitSpaceValues;
    }

    /** Returns <code>fitSpaceGammas</code>. */
    public Double[] getFitnessSpaceGammas() {
        return fitSpaceGammas;
    }

    /** Returns <code>genotypes</code>. */
    public int[][] getGenotypes() {
        return genotypes;
    }

    /**
     * Base implementation of marginal fitness effects.
     * <p>
     * Assumes multiplicative fitness effects across sites.
     */
    public double[][] getMarginalFitnessEffects(double[][] g) {

        // g normalized by sum at each site
        double[][] norm_g = new double[seqSites][];

        // fitness effect of each site weighted by norm_g
        double[] weightedFitEffects = new double[seqSites];

        double prodOfWeightedFitEffects = 1.0;

        for (int site=0; site<seqSites; site++) {
            norm_g[site] = new double[seqStates[site]];
            double siteSum = 0;
            for (int state=0; state<seqStates[site]; state++)
                siteSum += g[site][state];
            double w = 0; // weighted fitness effect of site
            for (int state=0; state<seqStates[site]; state++) {
                norm_g[site][state] = g[site][state] / siteSum;
                w += norm_g[site][state] * siteFitnessEffects[site][state];
            }
            weightedFitEffects[site] = w;
            prodOfWeightedFitEffects *= w;
        }

        // Marginal fitness at each site
        // site fitness effects marginalized over all other sites
        double[][] marginalFitEffects = new double[seqSites+1][];
        for (int site=0; site<seqSites; site++) {
            marginalFitEffects[site] = new double[seqStates[site]];
            for (int state=0; state<seqStates[site]; state++) {
                marginalFitEffects[site][state] =
                    siteFitnessEffects[site][state]
                    * prodOfWeightedFitEffects / weightedFitEffects[site];
            }
        }

        // Marginal fitness for entire lineage
        marginalFitEffects[seqSites] = new double[1];
        marginalFitEffects[seqSites][0] = prodOfWeightedFitEffects;

        return marginalFitEffects;
    }

    /**
     * Same as {@link #getMarginalFitnessEffects}, but works on {@link
     * SmallNumber} input.
     * <p>
     * <code>SmallNumber</code>s are only reverted back to doubles after
     * normalizing marginal site densities.
     */
    public double[][] getMarginalFitnessEffectsSN(SmallNumber[][] g) {

        double[][] norm_g = new double[seqSites][];
        double [] weightedFitEffects = new double[seqSites];
        double prodOfWeightedFitEffects = 1.0;

        for (int site=0; site<seqSites; site++) {
            norm_g[site] = new double[seqStates[site]];
            SmallNumber siteSum = new SmallNumber(0.0);
            for (int state=0; state<seqStates[site]; state++) {
                siteSum = SmallNumber.add(siteSum, g[site][state]);
            }
            double w = 0; // weighted fitness effect of site
            for (int state=0; state < seqStates[site]; state++) {
                // Convert back to double once normalized.
                norm_g[site][state] =
                    SmallNumber.divide(
                        g[site][state],
                        siteSum
                    ).revert();
                w += norm_g[site][state] * siteFitnessEffects[site][state];
            }
            weightedFitEffects[site] = w;
            prodOfWeightedFitEffects *= w;
        }

        double[][] marginalFitEffects = new double[seqSites+1][];
        for (int site=0; site<seqSites; site++) {
            marginalFitEffects[site] = new double[seqStates[site]];
            for (int state=0; state<seqStates[site]; state++)
                marginalFitEffects[site][state] =
                    siteFitnessEffects[site][state]
                    * prodOfWeightedFitEffects / weightedFitEffects[site];
        }
        marginalFitEffects[seqSites] = new double[1];
        marginalFitEffects[seqSites][0] = prodOfWeightedFitEffects;

        return marginalFitEffects;
    }

    @Override
    public void init(PrintStream out) {}

    @Override
    public void log(long sample, PrintStream out) {}

    @Override
    public void close(PrintStream out) {}

}
