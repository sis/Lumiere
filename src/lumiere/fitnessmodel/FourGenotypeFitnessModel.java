package lumiere.fitnessmodel;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.inference.parameter.RealParameter;

import java.util.ArrayList;


/**
 * Four-genotype fitness model where fitness depends on genotype of lineage.
 * <p>
 * For testing approximations of MFBD model.
 *
 * @author David Rasmussen
 */

@Description(
"Four-genotype fitness model where fitness depends on genotype of lineage. "
+ "For testing approximations of MFBD model.")

public class FourGenotypeFitnessModel extends MultiSiteFitnessModel {

    /** site-specific fitness effects (input) */
    public Input<RealParameter> fitEffectsInput = new Input<RealParameter>(
        "mutFitEffects",
        "site-specific fitness effects");

    /** points in fitness space to track */
    public Input<RealParameter> fitSpaceValuesInput = new Input<RealParameter>(
        "fitSpaceValues",
        "points in fitness space to track");

    /**
     * mutation rates
     * <p>
     * Flattened mutation matrix. Can be asymmetric. Diagonal entries omitted.
     */
    public Input<RealParameter> mutationMatrix = new Input<RealParameter>(
        "mutationMatrix",
        "Flattened mutation matrix. "
        + "Can be asymmetric. Diagonal entries omitted.");

    /** site-specific fitness effects (copy) */
    protected Double[] fitEffects;

    /** ... */
    protected int[][] genotypes;

    @Override
    public void setUp(Integer[] states, int sites) {

        int maxState = states[0];  // Assumes all sites have same # of states.
        seqStates = states;
        seqSites = sites;
        siteFitnessEffects = new double[seqSites][maxState];

        fitEffects = fitEffectsInput.get().getValues();

        // Only one "site" under this model since we're tracking all genotypes.
        siteFitnessEffects[0][0] = 1.0;            // genotype 00
        siteFitnessEffects[0][1] = fitEffects[0];  // genotype 10
        siteFitnessEffects[0][2] = fitEffects[1];  // genotype 01
        siteFitnessEffects[0][3] = fitEffects[0] * fitEffects[1];
            // genotype 11 -- assuming multiplicative fitness

        // Set up fitness space - values should be Double[]?
        fitSpaceValues = new Double[maxState];
        fitSpaceValues[0] = 1.0;
        fitSpaceValues[1] = fitEffects[0];
        fitSpaceValues[2] = fitEffects[1];
        fitSpaceValues[3] = fitEffects[0] * fitEffects[1];
        fitSpaceStates = fitSpaceValues.length;

        // Set up fitness space gammas.
        fitSpaceGammas = mutationMatrix.get().getValues();
    }

    @Override
    public void update() {

        fitEffects = fitEffectsInput.get().getValues();

        // Only one "site" under this model since we're tracking all genotypes.
        siteFitnessEffects[0][0] = 1.0;            // genotype 00
        siteFitnessEffects[0][1] = fitEffects[0];  // genotype 10
        siteFitnessEffects[0][2] = fitEffects[1];  // genotype 01
        siteFitnessEffects[0][3] = fitEffects[0] * fitEffects[1];
            // genotype 11 -- assuming multiplicative fitness

        // Set up fitness space.
        fitSpaceValues = new Double[seqStates[0]];
        fitSpaceValues[0] = 1.0;
        fitSpaceValues[1] = fitEffects[0];
        fitSpaceValues[2] = fitEffects[1];
        fitSpaceValues[3] = fitEffects[0] * fitEffects[1];
        fitSpaceStates = fitSpaceValues.length;

        // Set up fitness space gammas.
        fitSpaceGammas = mutationMatrix.get().getValues();
    }

    static ArrayList<Integer> indexOfAll(Integer element, ArrayList<Integer> list){
        ArrayList<Integer> indexList = new ArrayList<Integer>();
        for (int i = 0; i < list.size(); i++)
            if(element.equals(list.get(i)))
                indexList.add(i);
        return indexList;
    }

    static int hammingDistance(int[] genotype1, int[] genotype2) {
        int dist = 0;
        for(int i = 0; i < genotype1.length; i++) {
            if (genotype1[i] != genotype2[i]) dist++;
        }
        return dist;
    }

    static int mutationDirection(int[] genotype1, int[] genotype2) {
        int dist = 0;
        for(int i = 0; i < genotype1.length; i++) {
            dist += genotype1[i] - genotype2[i];
        }
        return dist;
    }

    @Override
    public int getClosestFitSpaceState(double fit) {
        double minAbsDistance = Double.POSITIVE_INFINITY;
        double absDiff = 0;
        int fitState = -1;
        for (int k=0; k<fitSpaceStates; k++) {
            absDiff = Math.abs(fitSpaceValues[k] - fit);
            if (absDiff < minAbsDistance) {
                fitState = k;
                minAbsDistance = absDiff;
            }
        }
        return fitState;
    }

}
