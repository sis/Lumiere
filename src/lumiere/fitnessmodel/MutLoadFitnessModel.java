package lumiere.fitnessmodel;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.inference.parameter.RealParameter;


/**
 * Multi-site fitness model where mutations at all sites have the same
 * deleterious effect <code>mutFitCost</code> on fitness.
 *
 * @author David Rasmussen
 */

@Description(
"Multi-site fitness model where mutations at all sites have the same "
+ "deleterious effect 'mutFitCost' on fitness.")

public class MutLoadFitnessModel extends MultiSiteFitnessModel {

    /** Selective cost of mutations, assumed invariant across sites. */
    public Input<RealParameter> fitCostInput = new Input<RealParameter>(
        "mutFitCost",
        "Selective cost of mutations, assumed invariant across sites.");

    /**
     * mutation rates
     * <p>
     * Flattened mutation matrix. Can be asymmetric. Diagonal entries omitted.
     */
    public Input<RealParameter> mutationMatrix = new Input<RealParameter>(
        "mutationMatrix",
        "Flattened mutation matrix. "
        + "Can be asymmetric. Diagonal entries omitted.");

    /** fitness cost */
    protected double fitCost;

    @Override
    public void setUp(Integer[] states, int sites) {

        int maxState = states[0];  // Assumes all sites have same # of states.
        seqStates = states;
        seqSites = sites;
        siteFitnessEffects = new double[seqSites][maxState];

        fitCost = fitCostInput.get().getValue();
        double forwardMutRate = mutationMatrix.get().getValues()[0];
        double backMutRate = mutationMatrix.get().getValues()[1];

        for (int k=0; k<seqSites; k++) {
            siteFitnessEffects[k][0] = 1.0;
            siteFitnessEffects[k][1] = 1.0 - fitCost;
        }

        // Set up fitness space.
        fitSpaceStates = seqSites + 1;  // Plus one for wildtype with no mutations.
        fitSpaceValues = new Double[fitSpaceStates];
        for (int k=0; k<fitSpaceStates; k++)
            fitSpaceValues[k] = Math.pow(1-fitCost,k);

        // Set up fitness space gammas.
        fitSpaceGammas = new Double[fitSpaceStates*(fitSpaceStates-1)];
        for (int i = 0; i < fitSpaceStates; i++) {
            for (int j = 0; j < fitSpaceStates; j++) {
                if (j == (i-1)) {
                    int index = i * (fitSpaceStates - 1) + j;
                        // Indexing is broken here
                    fitSpaceGammas[index] = backMutRate * i;
                        // back mutation from i --> j
                }
                else if (j == (i+1)) {
                    int index = i * (fitSpaceStates - 1) + (j-1);
                        // Indexing is broken here
                    fitSpaceGammas[index] = forwardMutRate * (seqSites - i);
                        // forward mutation from i --> j
                }
            }
        }

    }

    @Override
    public void update() {

        fitCost = fitCostInput.get().getValue();
        double forwardMutRate = mutationMatrix.get().getValues()[0];
        double backMutRate = mutationMatrix.get().getValues()[1];

        for (int k=0; k<seqSites; k++) {
            siteFitnessEffects[k][1] = 1.0 - fitCost;
        }

        // Update fitSpaceValues.
        for (int k=0; k<fitSpaceStates; k++) {
            fitSpaceValues[k] = Math.pow(1-fitCost,k);
        }

        // Update fitSpaceGammas (not necessary here).
        for (int i = 0; i < fitSpaceStates; i++) {
            for (int j = 0; j < fitSpaceStates; j++) {
                if (j == (i-1)) {
                    int index = i * (fitSpaceStates - 1) + j;
                        // Indexing is broken here.
                    fitSpaceGammas[index] = backMutRate * i;
                        // back mutation from i --> j
                }
                else if (j == (i+1)) {
                    int index = i * (fitSpaceStates - 1) + (j-1);
                        // Indexing is broken here.
                    fitSpaceGammas[index] = forwardMutRate * (seqSites - i);
                        // forward mutation from i --> j
                }
            }
        }

    }

    @Override
    public int getClosestFitSpaceState(double fit) {
        double minAbsDistance = Double.POSITIVE_INFINITY;
        double absDiff = 0;
        int fitState = -1;
        for (int k=0; k<fitSpaceStates; k++) {
            absDiff = Math.abs(fitSpaceValues[k] - fit);
            if (absDiff < minAbsDistance) {
                fitState = k;
                minAbsDistance = absDiff;
            }
        }
        return fitState;
    }

}
