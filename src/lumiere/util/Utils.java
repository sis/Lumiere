package lumiere.util;

import java.util.Arrays;


/**
 * Class providing helper functions.
 *
 * @author Denise Kuehnert (May 2012)
 */

public class Utils {

    /**
     * Returns the index of the time interval t lies in.
     * <p>
     * Does something similar as
     * {@link lumiere.distribution.PiecewiseFitnessBirthDeathDistribution#index}.
     */
    public static int index(Double t, Double[] times, int m) {
        int epoch = Arrays.binarySearch(times, t);
        if (epoch < 0)
            epoch = - epoch - 1;
        return Math.min(epoch, m - 1);
    }
}
