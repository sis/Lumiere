package lumiere.alignment;

import beast.base.core.Description;
import beast.base.core.Log;
import beast.base.evolution.alignment.Alignment;
import beast.base.evolution.alignment.Sequence;

import java.util.Arrays;
import java.util.List;


/**
 * Represents alignment data.
 * <p>
 * Derives from <code>beast.base.evolution.alignment.Alignment</code>, but does
 * not sort sites by site patterns.
 */

@Description(
"Class representing alignment data"
+ "Same as Alignment in beast.evolution.alignment but does not sort sites "
+ "by site patterns")

public class UnsortedAlignment extends Alignment
{
    /** maximum possible state value in data */
    protected int maxDataState;

    /** maximum possible state value at each site */
    protected Integer[] maxDataStates;

    /** No-op constructor. */
    public UnsortedAlignment() {}

    /** Constructor that simply delegates to the super class's constructor. */
    public UnsortedAlignment(List<Sequence> sequences, String dataType) {
        super(sequences, dataType);
    }

    /** Returns <code>maxDataState</code>. */
    public int getMaxDataState() {
        return maxDataState;
    }

    /** Returns <code>maxDataStates</code>. */
    public Integer[] getMaxDataStates() {
        return maxDataStates;
    }

    /** ... */
    @Override
    protected void calcPatterns(boolean log)
    {
        int taxonCount = counts.size();
        int siteCount = counts.get(0).size();

        // Convert data to transposed int array.
        int[][] data = new int[siteCount][taxonCount];
        maxDataState = 0;
        for (int i = 0; i < taxonCount; i++) {
            List<Integer> sites = counts.get(i);
            for (int j = 0; j < siteCount; j++) {
                data[j][i] = sites.get(j);
                if (sites.get(j) > maxDataState) {
                    maxDataState = sites.get(j);
                }
            }
        }

        // Add one because indexing states from zero.
        maxDataState = maxDataState + 1;

        // Find maxDataState for each site.
        maxDataStates = new Integer[siteCount];
        for (int j = 0; j < siteCount; j++) {
            int max = 0;
            for (int i = 0; i < taxonCount; i++) {
                if (data[j][i] > max) {
                    max = data[j][i];
                }
            }
            // Add one because indexing states from zero.
            maxDataStates[j] = max + 1;
        }

        // Sort data.
        SiteComparator comparator = new SiteComparator();

        // Count patterns in sorted data.
        // If (siteWeights != null) the weights are recalculated below.
        int patterns = 1;
        int[] weights = new int[siteCount];
        weights[0] = 1;
        for (int i = 1; i < siteCount; i++) {
            // In the case where we're using tip probabilities, we need to
            // treat each site as a unique pattern, because it could have a
            // unique probability vector.
            patterns++;
            data[patterns - 1] = data[i];
            weights[patterns - 1]++;
        }

        // Reserve memory for patterns.
        patternWeight = new int[patterns];
        sitePatterns = new int[patterns][taxonCount];
        for (int i = 0; i < patterns; i++) {
            patternWeight[i] = weights[i];
            sitePatterns[i] = data[i];
        }

        // Find patterns for the sites.
        patternIndex = new int[siteCount];
        for (int i = 0; i < siteCount; i++) {
            int[] sites = new int[taxonCount];
            for (int j = 0; j < taxonCount; j++)
                sites[j] = counts.get(j).get(i);
            patternIndex[i] = Arrays.binarySearch(sitePatterns, sites, comparator);
        }

        if (siteWeights != null) {
            Arrays.fill(patternWeight, 0);
            for (int i = 0; i < siteCount; i++)
                patternWeight[patternIndex[i]] += siteWeights[i];
        }

        // Determine maximum state count.
        // Usually, the state count is equal for all sites,
        // though for SnAP analysis, this is typically not the case.
        maxStateCount = 0;
        for (int m_nStateCount1 : stateCounts)
            maxStateCount = Math.max(maxStateCount, m_nStateCount1);

        // Report some statistics.
        if (log && taxaNames.size() < 30)
            for (int i = 0; i < taxaNames.size(); i++)
                Log.info.println(
                    taxaNames.get(i) + ": "
                    + counts.get(i).size() + " "
                    + stateCounts.get(i)
                );

        if (stripInvariantSitesInput.get()) {

            // Don't add patterns that are invariant, e.g. all gaps.
            if (log)
                Log.info.println("Stripping invariant sites.");

            int removedSites = 0;
            for (int i = 0; i < patterns; i++) {
                int[] pattern = sitePatterns[i];
                int value = pattern[0];
                boolean isInvariant = true;
                for (int k = 1; k < pattern.length; k++)
                    if (pattern[k] != value) {
                        isInvariant = false;
                        break;
                    }
                if (isInvariant) {
                    removedSites += patternWeight[i];
                    patternWeight[i] = 0;
                    if (log)
                        Log.info.print(" <" + value + "> ");
                }
            }
            if (log)
                Log.info.println(" removed " + removedSites + " sites ");
        }
    }
}
