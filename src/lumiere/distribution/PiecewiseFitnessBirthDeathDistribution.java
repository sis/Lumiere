package lumiere.distribution;

import lumiere.alignment.UnsortedAlignment;
import lumiere.fitnessmodel.MultiSiteFitnessModel;
import lumiere.math.MultiSiteScaledNumbers;
import lumiere.math.SmallNumber;
import lumiere.math.SmallNumberScaler;
import lumiere.math.p0_ODE;
import lumiere.math.p0ge_ODE;
import lumiere.math.p0ge_SiteConditions;
import lumiere.util.Utils;

import beast.base.core.Citation;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.inference.State;
import beast.base.inference.parameter.BooleanParameter;
import beast.base.inference.parameter.RealParameter;
import beast.base.evolution.alignment.Alignment;
import beast.base.evolution.speciation.SpeciesTreeDistribution;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.TreeInterface;

import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.ClassicalRungeKuttaIntegrator;
import org.apache.commons.math3.ode.nonstiff.DormandPrince54Integrator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;


/**
 * Marginal fitness birth-death model assuming piece-wise constant rates.
 * <p>
 * Piece-wise constant rates are assumed to be ordered by state and time.
 * First k entries of an array give values belonging to type 1, for intervals
 * 1 to k, second k intervals for type 2 etc."
 *
 * @author Denise (original implementation, 2014)
 * @author David Rasmussen (modified for the marginal fitness birth-death
 *         model, December 2017)
 */

@Citation(
"Rasmussen D.A. & Stadler T. 2019. " +
"Coupling adaptive molecular evolution to phylodynamics using " +
"fitness-dependent birth-death models" +
"Add ref.")

@Description(
"Piece-wise constant rates are assumed to be ordered by state and time. " +
"First k entries of an array give values belonging to type 1, for intervals " +
"1 to k, second k intervals for type 2 etc.")

public abstract class PiecewiseFitnessBirthDeathDistribution
    extends SpeciesTreeDistribution
{

    /** frequencies for each type */
    public Input<RealParameter> frequencies = new Input<RealParameter>(
        "frequencies",
        "frequencies for each type",
        Input.Validate.REQUIRED);

    /** origin of infection x1 */
    public Input<RealParameter> origin = new Input<RealParameter>(
        "origin",
        "origin of infection x1");

    /** The origin is only the length of the root edge. */
    public Input<Boolean> originIsRootEdge = new Input<Boolean>(
        "originIsRootEdge",
        "The origin is only the length of the root edge.",
        false);

    /** maximum number of evaluations for ODE solver */
    public Input<Integer> maxEvaluations = new Input<Integer>(
        "maxEvaluations",
        "maximum number of evaluations for ODE solver",
        1000000);

    /** Condition on at least one survival? */
    public Input<Boolean> conditionOnSurvival = new Input<Boolean>(
        "conditionOnSurvival",
        "Condition on at least one survival? Default: true.",
        true);

    /** relative tolerance for numerical integration */
    public Input<Double> relativeTolerance = new Input<Double>(
        "relTolerance",
        "relative tolerance for numerical integration",
        1e-7);

    /** absolute tolerance for numerical integration */
    public Input<Double> absoluteTolerance = new Input<Double>(
        "absTolerance",
        "absolute tolerance for numerical integration",
        1e-100);  // Double.MIN_VALUE

    /** times specifying when migration-rate changes occur */
    public Input<RealParameter> migChangeTimesInput = new Input<RealParameter>(
        "migChangeTimes",
        "times specifying when migration-rate changes occur",
        (RealParameter) null);

    /** times specifying when birth/R-rate changes occur */
    public Input<RealParameter> birthRateChangeTimesInput = new Input<RealParameter>(
        "birthRateChangeTimes",
        "times specifying when birth/R-rate changes occur",
        (RealParameter) null);

    /** times specifying when Rbirth/R-among-demes changes occur */
    public Input<RealParameter> b_ijChangeTimesInput = new Input<RealParameter>(
        "birthRateAmongDemesChangeTimes",
        "times specifying when Rbirth/R-among-demes changes occur",
        (RealParameter) null);

    /** times specifying when death-rate changes occur */
    public Input<RealParameter> deathRateChangeTimesInput = new Input<RealParameter>(
        "deathRateChangeTimes",
        "times specifying when death/becomeUninfectious rate changes occur",
        (RealParameter) null);

    /** times specifying when sampling-rate or sampling-proportion changes occur */
    public Input<RealParameter> samplingRateChangeTimesInput = new Input<RealParameter>(
        "samplingRateChangeTimes",
        "times specifying when sampling-rate or sampling-proportion changes occur",
        (RealParameter) null);

    /** times specifying when removal-probability changes occur */
    public Input<RealParameter> removalProbabilityChangeTimesInput = new Input<RealParameter>(
        "removalProbabilityChangeTimes",
        "times specifying when removal-probability changes occur",
        (RealParameter) null);

    /** interval times for all parameters if they are the same */
    public Input<RealParameter> intervalTimes = new Input<RealParameter>(
        "intervalTimes",
        "interval times for all parameters if they are the same",
        (RealParameter) null);

    /** Migration-rate change times specified relative to tree height? */
    public Input<Boolean> migTimesRelativeInput = new Input<Boolean>(
        "migTimesRelative",
        "Migration-rate change times specified relative to tree height? "
        + "Default: false.",
        false);

    /** Birth-rate change times specified relative to tree height? */
    public Input<Boolean> b_ijChangeTimesRelativeInput = new Input<Boolean>(
        "birthRateAmongDemesTimesRelative",
        "Birth-rate change times specified relative to tree height? "
        + "Default: false.",
        false);

    /** Birth-rate change times specified relative to tree height? */
    public Input<Boolean> birthRateChangeTimesRelativeInput = new Input<Boolean>(
        "birthRateTimesRelative",
        "Birth-rate change times specified relative to tree height? "
        + "Default false.",
        false);

    /** Death-rate change times specified relative to tree height? */
    public Input<Boolean> deathRateChangeTimesRelativeInput = new Input<Boolean>(
        "deathRateTimesRelative",
        "Death-rate change times specified relative to tree height? "
        + "Default: false.",
        false);

    /** Sampling-rate times specified relative to tree height? */
    public Input<Boolean> samplingRateChangeTimesRelativeInput = new Input<Boolean>(
        "samplingRateTimesRelative",
        "Sampling-rate times specified relative to tree height? "
        + "Default: false.",
        false);

    /** Removal-probability change times specified relative to tree height? */
    Input<Boolean> removalProbabilityChangeTimesRelativeInput = new Input<Boolean>(
        "removalProbabilityTimesRelative",
        "Removal-probability change times specified relative to tree height? "
        + "Default: false.",
        false);

    /**
     * Time arrays given in backwards time?
     * <p>
     * That means from the present back to root.
     * Order: 1) birth 2) death 3) sampling 4) rho 5) r 6) migration.
     * <p>
     * Careful, rate array must still be given in <em>forward</em> time
     * (root to tips).
     */
    public Input<BooleanParameter> reverseTimeArraysInput = new Input<BooleanParameter>(
        "reverseTimeArrays",
        "Time arrays given in backwards time? "
        + "That means from the present back to root."
        + "Order: 1) birth 2) death 3) sampling 4) rho 5) r 6) migration. "
        + "Default false. "
        + "Careful, rate array must still be given in FORWARD time "
        + "(root to tips).");

    /** Times specifying when rho sampling occurs. */
    public Input<RealParameter> rhoSamplingTimes = new Input<RealParameter>(
        "rhoSamplingTimes",
        "Times specifying when rho sampling occurs.",
        (RealParameter) null);

    /**
     * Only contemporaneous sampling?
     * <p>
     * I.e. all tips are from the same sampling time.
     */
    public Input<Boolean> contemp = new Input<Boolean>(
        "contemp",
        "Only contemporaneous sampling? " +
        "I.e. all tips are from the same sampling time."
        + "Default: false.",
        false);

    /**
     * birthRate = birthRateVector * birthRateScalar
     * <p>
     * Birth rate can change over time.
     */
    public Input<RealParameter> birthRate = new Input<RealParameter>(
        "birthRate",
        "birthRate = birthRateVector * birthRateScalar; "
        + "Birth rate can change over time.");

    /** The deathRate vector with birthRates between times. */
    public Input<RealParameter> deathRate = new Input<RealParameter>(
        "deathRate",
        "The deathRate vector with birthRates between times.");

    /** sampling rate per individual */
    public Input<RealParameter> samplingRate = new Input<RealParameter>(
        "samplingRate",
        "sampling rate per individual");

    /** proportion of lineages sampled at rho-sampling times */
    public Input<RealParameter> m_rho = new Input<RealParameter>(
        "rho",
        "The proportion of lineages sampled at rho-sampling times. "
        + "Default: 0.");

    /** basic reproduction number */
    public Input<RealParameter> R0 = new Input<RealParameter>(
        "R0",
        "basic reproduction number");

    /** rate at which individuals become uninfectious
        (through recovery or sampling) */
    public Input<RealParameter> becomeUninfectiousRate = new Input<RealParameter>(
        "becomeUninfectiousRate",
        "rate at which individuals become uninfectious "
        + "(through recovery or sampling)",
        Input.Validate.XOR, deathRate);

    /** samplingProportion = samplingRate / becomeUninfectiousRate */
    public Input<RealParameter> samplingProportion = new Input<RealParameter>(
        "samplingProportion",
        "samplingProportion = samplingRate / becomeUninfectiousRate",
        Input.Validate.XOR, samplingRate);

    /** Should all types should have the same 1) birth 2) death
        3) sampling 4) rho 5) r 6) migration rate? */
    public Input<BooleanParameter> identicalRatesForAllTypesInput = new Input<BooleanParameter>(
        "identicalRatesForAllTypes",
        "Should all types should have the same 1) birth 2) death "
        + "3) sampling 4) rho 5) r 6) migration rate? Default: false.");

    /**
     * base reproduction number for the base pathogen class
     * <p>
     * Should have the same dimension as the number of time intervals.
     */
    public Input<RealParameter> R0_base = new Input<RealParameter>(
        "R0_base",
        "The base reproduction number for the base pathogen class. "
        + "Should have the same dimension as the number of time intervals.");

    /**
     * ratio of basic infection rates of all other classes compared to
     * the base lambda
     * <p>
     * Should have the dimension of the number of pathogens - 1,
     * as it is kept constant over intervals.
     */
    public Input<RealParameter> lambda_ratio = new Input<RealParameter>(
        "lambda_ratio",
        "The ratio of basic infection rates of all other classes "
        + "when compared to the base lambda. "
        + "Should have the dimension of the number of pathogens - 1, "
        + "as it is kept constant over intervals.");

    /**
     * flattened migration matrix
     *<p>
     * Can be asymmetric. Diagonal entries omitted.
     */
    public Input<RealParameter> migrationMatrix = new Input<RealParameter>(
        "migrationMatrix",
        "Flattened migration matrix. "
        + "Can be asymmetric. Diagonal entries omitted.");

    /** A real number with which each migration rate entry is scaled. */
    public Input<RealParameter> migrationMatrixScaleFactor = new Input<RealParameter>(
        "migrationMatrixScaleFactor",
        "A real number with which each migration rate entry is scaled.");

    /** birth rate vector with rate at which transmissions occur among
        locations */
    public Input<RealParameter> birthRateAmongDemes = new Input<RealParameter>(
        "birthRateAmongDemes",
        "birth rate vector with rate at which transmissions occur "
        + "among locations");

    /** base reproduction number determining when transmissions occur
        among locations */
    public Input<RealParameter> R0AmongDemes = new Input<RealParameter>(
        "R0AmongDemes",
        "base reproduction number determining when transmissions occur "
        + "among locations");

    /** probability of an individual to become non-infectious
        immediately after the sampling */
    public Input<RealParameter> removalProbability = new Input<RealParameter>(
        "removalProbability",
        "probability of an individual to become non-infectious "
        + "immediately after the sampling");

    /** number of states or locations */
    public Input<Integer> stateNumber = new Input<Integer>(
        "stateNumber",
        "number of states or locations",
        Input.Validate.REQUIRED);

    /** origin of MASTER sims which has to be deducted from the
        change-time arrays */
    public Input<RealParameter> adjustTimesInput = new Input<RealParameter>(
        "adjustTimes",
        "origin of MASTER sims which has to be deducted from the "
        + "change-time arrays");
        // HACK ALERT. For reestimation from MASTER sims, adjustTimes is used
        // to correct the forward changetimes such that they don't include
        // orig-root (when we're not estimating the origin).

    /** Use fixed step size Runge-Kutta integrator with 1000 steps? */
    public Input<Boolean> useRKInput = new Input<Boolean>(
        "useRK",
        "Use fixed step size Runge-Kutta integrator with 1000 steps? "
        + "Default: false.",
        false);

    /** Check if rho is set if multiple tips are given at present? */
    public Input<Boolean> checkRho = new Input<Boolean>(
        "checkRho",
        "Check if rho is set if multiple tips are given at present? "
        + "Default: true.",
        true);

    /** time step for numerical integration of likelihood */
    public Input<Double> dtTimeStepInput = new Input<Double>(
        "dtTimeStep",
        "time step for numerical integration of likelihood",
        1e-6);

    /** Approximate E (p0) probabilities in fitness space? */
    public Input<Boolean> approxEProbsInput = new Input<Boolean>(
        "approxEProbs",
        "Approximate E (p0) probabilities in fitness space? "
        + "Default: true.",
        true);

    /** the multi-site fitness model */
    public Input<MultiSiteFitnessModel> fitnessModelInput = new Input<MultiSiteFitnessModel>(
        "fitnessModel",
        "the multi-site fitness model",
        Input.Validate.REQUIRED);

    /** sequence alignment */
    public Input<Alignment> alignInput = new Input<Alignment>(
        "alignment",
        "sequence alignment");

    /** To do: Check if we can set this back to 1e-20. */
    public final static double globalPrecisionThreshold = 1e-10;

    double T;
    double orig;
    int ntaxa;

    p0_ODE P;
    p0ge_ODE PG;

    FirstOrderIntegrator pg_integrator;

    /** ... */
    public int maxEvalsUsed;

    /** ... */
    public Double minstep;

    /** ... */
    public Double maxstep;

    /** ... */
    public boolean SAModel;

    /** ... */
    protected Double[] birth;

    // These four arrays are totalIntervals in length.
    Double[] death;
    Double[] psi;
    Double[] rho;
    Double[] r;

    // The number of change points in the birth rate, b_ij, death rate,
    // sampling rate, rho, r.
    int migChanges;
    int birthChanges;
    int b_ij_Changes;
    int deathChanges;
    int samplingChanges;
    int rhoChanges;
    int rChanges;

    int rhoSamplingCount;               // number of times rho-sampling occurs

    Boolean constantRho;
    Boolean[] isRhoTip;
    Boolean[] isRhoInternalNode;

    int totalIntervals;                 // total interval count
    int n;                              // number of states / locations

    protected List<Double> migChangeTimes          = new ArrayList<>();
    protected List<Double> birthRateChangeTimes    = new ArrayList<>();
    protected List<Double> b_ijChangeTimes         = new ArrayList<>();
    protected List<Double> deathRateChangeTimes    = new ArrayList<>();
    protected List<Double> samplingRateChangeTimes = new ArrayList<>();
    protected List<Double> rhoSamplingChangeTimes  = new ArrayList<>();
    protected List<Double> rChangeTimes            = new ArrayList<Double>();

    Boolean contempData;
    SortedSet<Double> timesSet = new TreeSet<>();

    /** ... */
    protected Double[] times = new Double[]{0.};

    /** .. */
    protected Boolean transform;

    Boolean migTimesRelative          = false;
    Boolean birthRateTimesRelative    = false;
    Boolean b_ijTimesRelative         = false;
    Boolean deathRateTimesRelative    = false;
    Boolean samplingRateTimesRelative = false;
    Boolean rTimesRelative            = false;
    Boolean birthAmongDemes           = false;

    Boolean[] reverseTimeArrays;
    Double[] M;
    Double[] b_ij;
    Double[] freq;
    double[][] pInitialConditions;

    // For tracking p0 as a time series.
    Double[] integrationTimes;
    double[][] pConditions;

    public MultiSiteFitnessModel fitnessModel;

    /** Added for fast Euler integration. */
    public Double dtTimeStep;

    /** Is sampling coupled to removal? */
    public Boolean samplingCoupledToRemoval = false;

    /** ... */
    public Boolean approxEProbs = true;

    /** ... */
    public UnsortedAlignment seqData;

    /** ... */
    public int siteCount;

    /** ... */
    public Integer[] seqStates;

    /** ... */
    public int maxState;

    Boolean[] identicalRatesForAllTypes;

    /** Instantiates the class based on the input parameters. */
    @Override
    public void initAndValidate() {

        Alignment data = alignInput.get();

        // Get sequencing data from alignment for each tip.
        seqData = new UnsortedAlignment(
            data.sequenceInput.get(),
            data.dataTypeInput.get()
        );

        siteCount = seqData.getPatternCount();   // number of evolving sites
        maxState = seqData.getMaxDataState();    // maximum number of states at each site
        seqStates = seqData.getMaxDataStates();  // number of states at each site

        // Set up fitness model.
        fitnessModel = fitnessModelInput.get();
        fitnessModel.setUp(seqStates, siteCount);

        // Set time step for numerical integration.
        dtTimeStep = dtTimeStepInput.get();

        identicalRatesForAllTypes
            = new Boolean[] { false, false, false, false, false, false };
        if (identicalRatesForAllTypesInput.get() != null)
            identicalRatesForAllTypes
                = identicalRatesForAllTypesInput.get().getValues();
        if (removalProbability.get() != null)
            SAModel = true;
        if (approxEProbsInput.get()!=null)
            approxEProbs = approxEProbsInput.get();

        birth = null;
        b_ij = null;
        death = null;
        psi = null;
        rho = null;
        r = null;
        birthRateChangeTimes.clear();
        deathRateChangeTimes.clear();
        samplingRateChangeTimes.clear();
        if (SAModel)
            rChangeTimes.clear();
        totalIntervals = 0;
        n = stateNumber.get();

        birthAmongDemes =
            (
                birthRateAmongDemes.get() != null
                || R0AmongDemes.get() != null
            )
            && n > 1;

        Double factor;
        if (migrationMatrix.get() != null) {
            M = migrationMatrix.get().getValues();
            if (migrationMatrixScaleFactor.get() != null) {
                factor = migrationMatrixScaleFactor.get().getValue();
                for (int i = 0; i < M.length; i++)
                    M[i] *= factor;
            }

            if (n > 1 && M.length != n * (n - 1)) {
                double timeChanges = 0;
                if (migChangeTimesInput.get() != null)
                    timeChanges = migChangeTimesInput.get().getDimension();
                else if (intervalTimes.get() != null)
                    timeChanges = intervalTimes.get().getDimension();
                if (timeChanges == 0 || M.length != n * (n - 1) * timeChanges)
                    throw new RuntimeException(
                        "Migration matrix dimension is incorrect!");
            }
            migChanges = migrationMatrix.get().getDimension()
                         /
                         Math.max(1 , (maxState * (maxState - 1))) - 1;
                         // migrationMatrix has dimension n = seqStates.
        }
        else if (!birthAmongDemes)
            throw new RuntimeException(
                "Error in BDMM setup: Need to specify at least one of the "
                + "following: migrationMatrix, R0AmongDemes, birthRateAmongDemes.");

        birthRateTimesRelative    = birthRateChangeTimesRelativeInput.get();
        b_ijTimesRelative         = b_ijChangeTimesRelativeInput.get();
        migTimesRelative          = migTimesRelativeInput.get();
        deathRateTimesRelative    = deathRateChangeTimesRelativeInput.get();
        samplingRateTimesRelative = samplingRateChangeTimesRelativeInput.get();

        if (SAModel)
            rTimesRelative = removalProbabilityChangeTimesRelativeInput.get();

        reverseTimeArrays =
            new Boolean[] {false, false, false, false, false, false};
        if (reverseTimeArraysInput.get() != null) {
            Boolean[] r = reverseTimeArraysInput.get().getValues();
            for (int i = 0; i < r.length; i++)
            {
                reverseTimeArrays[i] = r[i];
            }
        }

        rhoSamplingCount = 0;
        contempData = contemp.get();

        if (
            birthRate.get() == null
            && R0.get() == null
            && R0_base.get() == null
            && lambda_ratio.get() == null
        )
            throw new RuntimeException(
                "Either birthRate, R0, or R0_base and R0_ratio "
                + "need to be specified!");
        else if (
            (birthRate.get() != null && R0.get() != null)
            || (
                R0.get() != null
                && (R0_base.get() != null || lambda_ratio.get() != null)
            )
            || (
                birthRate.get() != null
                && (R0_base.get() != null || lambda_ratio.get() != null)
            )
        )
            throw new RuntimeException(
                "Only one of birthRate, or R0, or R0_base and "
                + "lambda_ratio need to be specified!");
        else if (
            birthRate.get() != null
            && deathRate.get() != null
            && samplingRate.get() != null
        ) {

            // For condition where birthRate, deathRate and samplingProportion
            // is defined. In this case psi = samplingRate.

            // Using untransformed parameterization.
            transform = false;
            death = deathRate.get().getValues();
            psi   = samplingRate.get().getValues();
            birth = birthRate.get().getValues();
            if (SAModel)
                r = removalProbability.get().getValues();

            if (birthRateAmongDemes.get()!=null && n > 1)
            {
                birthAmongDemes = true;
                b_ij = birthRateAmongDemes.get().getValues();
            }
        }
        else if (
            birthRate.get() != null
            && deathRate.get() != null
            && samplingProportion.get() != null
        ) {

            // For condition where birthRate, deathRate and samplingProportion
            // is defined. In this case psi = samplingProportion

            // Using untransformed parameterization.
            transform = false;
            samplingCoupledToRemoval = true;
                // Sampling must be coupled to removal under this
                // parameterization.
            death = deathRate.get().getValues();
            psi   = samplingProportion.get().getValues();
            birth = birthRate.get().getValues();
            if (SAModel)
                r = removalProbability.get().getValues();

            if (birthRateAmongDemes.get() != null && n > 1)
            {
                birthAmongDemes = true;
                b_ij = birthRateAmongDemes.get().getValues();
            }
        }
        else if (
            (
                R0.get() != null
                || (R0_base.get() != null && lambda_ratio.get() != null)
            )
            && becomeUninfectiousRate.get() != null
            && samplingProportion.get() != null
        )
            transform = true;
        else
            throw new RuntimeException(
                "Either specify birthRate, deathRate and samplingRate "
                + "OR specify R0 (or R0_base AND R0_ratio), "
                + "becomeUninfectiousRate and samplingProportion!");

        if (transform) {
            if (R0AmongDemes.get() != null && n > 1) {
                birthAmongDemes = true;
                b_ij_Changes = R0AmongDemes.get().getDimension()
                               /
                               Math.max(1, (n*(n-1))) - 1;
            }

            if (birthChanges < 1)
                if (R0.get() != null)
                    birthChanges = R0.get().getDimension() / n - 1;
                else
                    birthChanges = R0_base.get().getDimension() - 1;
            samplingChanges = samplingProportion.get().getDimension() / n - 1;
            deathChanges = becomeUninfectiousRate.get().getDimension() / n - 1;
        }
        else {
            // To do: b d s param doesn't work yet with rate changes
            // (unless all parameters have equally many).
            if (birthChanges < 1)
                birthChanges = birthRate.get().getDimension() / n - 1;
            if (birthAmongDemes)
                b_ij_Changes = birthRateAmongDemes.get().getDimension()
                               /
                               (n*(n-1)) - 1;
            deathChanges = deathRate.get().getDimension() / n - 1;
            if (samplingCoupledToRemoval)
                samplingChanges = samplingProportion.get().getDimension() / n - 1;
            else
                samplingChanges = samplingRate.get().getDimension() / n - 1;
        }

        if (SAModel)
            rChanges = removalProbability.get().getDimension() / n - 1;

        if (m_rho.get() != null) {
            rho = m_rho.get().getValues();
            rhoChanges = m_rho.get().getDimension() / n - 1;
        }

        freq = frequencies.get().getValues();

        double freqSum = 0;
        for (double f : freq)
            freqSum += f;
        if (freqSum != 1.0)
            throw new RuntimeException(
                "Error: frequencies must add up to 1 "
                + "but currently add to " + freqSum + ".");
    }

    /**
     * Solves ge equations.
     * <p>
     * These are the D_{N} probabilities in the notation of Stadler and
     * Bonhoeffer, 2013. Assumes entire time series of p0 is known: these are
     * the E probabilities in the notation of Stadler and Bonhoeffer, 2013.
     * Signs (+/-) on derivatives are reversed.
     *
     * @param g
     * @param to
     * @param from
     * @param index
     */
    public double[][] fastEulerIntegrateGxP0(
        double[][] g, double to, double from, int index)
    {

        int k, l;

        double[][] gDot = new double[siteCount + 1][]; // Holds derivatives.
        int[][] genotypes = fitnessModel.getGenotypes();
        double[][] marginalSiteProbs = getMarginalSiteProbs(g);

        // We are integrating backwards in time, starting at `to` / `nextTo`
        // back to `from`, so dt will always be negative.

        double nextTo = to - dtTimeStep;
        double dt = nextTo - to;        // Should be negative.
        to = nextTo;                    // Do we need this other `to`?
        boolean stepBack = true;
        while (stepBack) {

            if (nextTo < from) {
                dt = nextTo - from;
                stepBack = false;
            }

            // Get p0 conditions for this time point.
            int p0Index = Utils.index(to, integrationTimes, integrationTimes.length);
            double[] p0 = pConditions[integrationTimes.length - p0Index - 1];

            // Update marginal fit effects.
            double[][] marginalFitEffects = fitnessModel.getMarginalFitnessEffects(g);
            if (!approxEProbs)
                marginalSiteProbs = getMarginalSiteProbs(g);
                // Only need this if approxEProbs == false.

            for (int site = 0; site <= siteCount; site++) {
                if (site == siteCount) {
                    // Final site represents entire lineage for
                    // computing p(T | S, \theta).
                    gDot[site] = new double[1]; // Only one "state".
                    k = index;

                    // expected birth rate based on marginal fitness
                    double lambda = birth[k] * marginalFitEffects[siteCount][0];

                    // expected p0 for entire lineage
                    double expected_p0;

                    if (approxEProbs) {
                        int fitClass =
                            fitnessModel.getClosestFitSpaceState(
                                marginalFitEffects[site][0]
                            );
                        expected_p0 = p0[fitClass];
                    }
                    else {
                        expected_p0 = 0;
                        double[] gpr = new double[genotypes.length];
                        double gprSum = 0;
                        for (int geno = 0; geno < genotypes.length; geno++) {
                            gpr[geno] = 1.0;
                            for (int j = 0; j < siteCount; j++) {
                                int type = genotypes[geno][j];
                                gpr[geno] *= marginalSiteProbs[j][type];
                            }
                            gprSum += gpr[geno];
                        }

                        if (gprSum <= 0)
                            for (int geno = 0; geno < genotypes.length; geno++)
                                expected_p0 += p0[geno] / genotypes.length;
                        else
                            for (int geno = 0; geno < genotypes.length; geno++)
                                expected_p0 += p0[geno] * gpr[geno] / gprSum;
                    }

                    if (samplingCoupledToRemoval)
                        gDot[site][0] =
                            (lambda + death[k] - 2 * lambda * expected_p0)
                            * g[site][0];
                    else
                        gDot[site][0] =
                            (lambda + death[k] + psi[k]
                             - 2 * lambda * expected_p0)
                            * g[site][0];

                    // Update g according to derivatives in gDot.
                    g[site][0] += gDot[site][0] * dt;
                }
                else {
                    // For a given site.
                    gDot[site] = new double[seqStates[site]];

                    // Loop over all seqState.
                    for (int i = 0; i < seqStates[site]; i++) {
                        k = index;

                        // expected birth rate based on marginal fitness
                        double lambda = birth[k] * marginalFitEffects[site][i];

                        double expected_p0;

                        if (approxEProbs) {
                            // Default is true so old code will run as before.
                            int fitClass
                                = fitnessModel.getClosestFitSpaceState(
                                    marginalFitEffects[site][i]);
                            expected_p0 = p0[fitClass];
                        }
                        else {
                            expected_p0 = 0;
                            double[] gpr = new double[genotypes.length];
                            double gprSum = 0;
                            for (int geno = 0; geno < genotypes.length; geno++) {
                                if (genotypes[geno][site] == i) {
                                    // Otherwise no probability of being in
                                    // this genotype.
                                    gpr[geno] = 1.0;
                                    for (int j = 0; j < siteCount; j++) {
                                        if (site != j) {
                                            // Already accounted for this site above.
                                            int type = genotypes[geno][j];
                                            gpr[geno] *= marginalSiteProbs[j][type];
                                        }
                                    }
                                    gprSum += gpr[geno];
                                }
                            }

                            if (gprSum <= 0)
                                for (int geno = 0; geno < genotypes.length; geno++)
                                    expected_p0 += p0[geno] / genotypes.length;
                            else {
                                for (int geno = 0; geno < genotypes.length; geno++)
                                    expected_p0 += p0[geno] * gpr[geno] / gprSum;
                            }
                        }

                        if (samplingCoupledToRemoval)
                            gDot[site][i] =
                                (lambda + death[k] - 2 * lambda * expected_p0)
                                * g[site][i];
                        else
                            gDot[site][i] =
                                (lambda+death[k] + psi[k]
                                 - 2 * lambda * expected_p0)
                                * g[site][i];

                        // Loop over all other states j.
                        for (int j = 0; j < seqStates[site]; j++) {

                            l = (i * (maxState - 1) + (j < i ? j : j - 1))
                                * totalIntervals
                                + index;

                            if (i != j)
                                if (M[0] != null) {
                                    // mutation
                                    gDot[site][i] += M[l] * g[site][i];
                                        // Probability of no mutation from i to j.
                                    gDot[site][i] -= M[l] * g[site][j];
                                        // Mutation from i to j.
                                }
                        }
                    }

                    // Update g according to derivatives in gDot.
                    for (int i = 0; i < seqStates[site]; i++)
                        g[site][i] += gDot[site][i] * dt;
                }
            }

            // Update trackers.
            nextTo = to - dtTimeStep;
            dt = nextTo - to;
            to = nextTo;
        }

        return g;
    }

    /**
     * Returns the (normalized) marginal site probabilities for all sites.
     * <p>
     * g is normalized by the sum at each site.
     *
     * @param g
     * @return site probabilities
     */
    public double[][] getMarginalSiteProbs(double[][] g) {
        double[][] norm_g = new double[siteCount][];
        for (int site = 0; site < siteCount; site++) {
            norm_g[site] = new double[seqStates[site]];
            double siteSum = 0;
            for (int state = 0; state < seqStates[site]; state++)
                siteSum += g[site][state];
            for (int state = 0; state < seqStates[site]; state++)
                norm_g[site][state] = g[site][state] / siteSum;
        }
        return norm_g;
    }

    /**
     * Same as {@link #getMarginalSiteProbs} but works on {@link SmallNumber}
     * input.
     * <p>
     * <code>SmallNumber</code> objects are only reverted back to doubles
     * after normalizing marginal site densities.
     *
     * @param g
     * @return site probabilities
     */

    public double[][] getMarginalSiteProbsSN(SmallNumber[][] g) {
        double[][] norm_g = new double[siteCount][];
        for (int site = 0; site < siteCount; site++) {
            norm_g[site] = new double[seqStates[site]];
            SmallNumber siteSum = new SmallNumber(0.0);
            for (int state = 0; state < seqStates[site]; state++) {
                siteSum = SmallNumber.add(siteSum, g[site][state]);
            }
            for (int state = 0; state < seqStates[site]; state++) {
                // Convert back to double once normalized.
                norm_g[site][state]
                    = SmallNumber.divide(
                        g[site][state],
                        siteSum
                    ).revert();
            }
        }
        return norm_g;
    }

    /**
     * Fast implementation of <code>getG</code> using Euler integration.
     * <p>
     * Implementation of getG with {@link SmallNumber} objects for the ge
     * equations. Avoids underflowing of integration results.
     *
     * @param t
     * @param PG0
     * @param t0
     * @param pg_integrator
     * @param T
     * @param maxEvalsUsed
     */
    public p0ge_SiteConditions getGFastEuler(
        double t,
        p0ge_SiteConditions PG0,
        double t0,
        FirstOrderIntegrator pg_integrator,
        Double T,
        int maxEvalsUsed
    ) {
        // PG0 contains initial condition for p0 (0..n-1) and for ge (n..2n-1).
        try {
            if (
                Math.abs(T-t) < globalPrecisionThreshold
                || Math.abs(t0-t) < globalPrecisionThreshold
                || T < t
            )
                return PG0;

            double from = t;
            double to = t0;
            double oneMinusRho;

            int indexFrom = Utils.index(from, times, times.length);
            int index = Utils.index(to, times, times.length);

            int steps = index - indexFrom;
            if (Math.abs(from - times[indexFrom]) < globalPrecisionThreshold)
                steps--;
            if (
                index > 0
                && Math.abs(to - times[index - 1]) < globalPrecisionThreshold
            ) {
                steps--;
                index--;
            }
            index--;

            // pgScaled contains the set of initial conditions scaled
            // made to fit the requirements on the values `double` can
            // represent. It also contains the factor by which the numbers
            // were multiplied.
            MultiSiteScaledNumbers pgScaled = SmallNumberScaler.scaleAllSites(PG0);

            // integrationResults will temporarily store the results of
            // each integration step as `doubles`, before converting them
            // back to `SmallNumbers`.
            double[] p0IntegrationResults = new double[fitnessModel.getFitSpaceStates()];
            double[][] gIntegrationResults = new double[siteCount + 1][];
            for (int site = 0; site < siteCount; site++)
                gIntegrationResults[site] = new double[seqStates[site]];
            gIntegrationResults[siteCount] = new double[1];

            while (steps > 0) {
                from = times[index];
                gIntegrationResults = fastEulerIntegrateGxP0(
                        pgScaled.getEquation(), to, from, index
                );
                PG0 = SmallNumberScaler.unscaleAllSites(
                        p0IntegrationResults,
                        gIntegrationResults,
                        pgScaled.getScalingFactor()
                );

                // Needs to be done for each site as well.
                if (rhoChanges > 0) {
                    oneMinusRho = 1 - rho[index];
                    for (int site = 0; site <= siteCount; site++)
                        if (site == siteCount)
                            PG0.conditionsOnG[site][0]
                                = PG0.conditionsOnG[site][0]
                                  .scalarMultiply(oneMinusRho);
                        else
                            for (int i = 0; i < seqStates[site]; i++)
                                PG0.conditionsOnG[site][i]
                                    = PG0.conditionsOnG[site][i]
                                      .scalarMultiply(oneMinusRho);
                }

                to = times[index];

                steps--;
                index--;

                // "Rescale" the results of the last integration
                // to prepare for the next integration step.
                pgScaled = SmallNumberScaler.scaleAllSites(PG0);
            }

            // Integrate backwards starting with "to" back to final "from".
            gIntegrationResults = fastEulerIntegrateGxP0(
                pgScaled.getEquation(), to, from,index + 1);
            PG0 = SmallNumberScaler.unscaleAllSites(
                p0IntegrationResults,
                gIntegrationResults,
                pgScaled.getScalingFactor()
            );
        }
        catch(Exception e) {
            throw new RuntimeException("Couldn't calculate g.");
        }

        return PG0;
    }

    /** ... */
    void setRho() {
        isRhoTip = new Boolean[treeInput.get().getLeafNodeCount()];
        Arrays.fill(isRhoTip, false);

        isRhoInternalNode = new Boolean[treeInput.get().getInternalNodeCount()];
        Arrays.fill(isRhoInternalNode, false);

        if (m_rho.get() != null) {
            constantRho = !(m_rho.get().getDimension() > n);

            if (
                m_rho.get().getDimension() <= n
                && (
                    rhoSamplingTimes.get()==null
                    || rhoSamplingTimes.get().getDimension() < 2)
            ) {
                if (!contempData
                    && (
                        (samplingProportion.get() != null
                            && samplingProportion.get().getDimension() <= n
                            && samplingProportion.get().getValue() == 0.
                        )
                        || (
                            // To do: Instead of
                            // samplingProportion.get().getValue() == 0,
                            // check that samplingProportion[i]==0
                            // for all i = 0 ... n-1.
                            samplingRate.get() != null
                            && samplingRate.get().getDimension() <= 2
                            && samplingRate.get().getValue() == 0.
                        )
                    )
                ) {
                    // To do: Instead of samplingRate.get().getValue() == 0,
                    // check that samplingRate[i]==0 for all i = 0 ... n-1.
                    // Check if data set is contemp!
                    for (Node node : treeInput.get().getExternalNodes())
                        if (node.getHeight() > 0.0)
                            throw new RuntimeException(
                                "Error in analysis setup: Parameters set for "
                                + "entirely contemporaneously sampled data, "
                                + "but some nodeheights are > 0!");

                    contempData = true;
                    System.out.println("BDMM: setting contemp=true.");
                }
            }

            if (contempData) {
                if (
                    m_rho.get().getDimension() != 1
                    && m_rho.get().getDimension() != n
                )
                    throw new RuntimeException(
                        "When contemp=true, rho must have dimension 1 "
                        + "(or be equal to the stateNumber).");
                else {
                    rho = new Double[n*totalIntervals];
                    Arrays.fill(rho, 0.);
                    Arrays.fill(isRhoTip, true);
                    for (int i = 1; i <= n; i++)
                        rho[i*totalIntervals - 1] = m_rho.get().getValue(i-1);
                    rhoSamplingCount = 1;
                }
            }
            else {
                Double[] rhos = m_rho.get().getValues();
                rho = new Double[n * totalIntervals];
                Arrays.fill(rho, 0.0);
                for (int i = 0; i < totalIntervals; i++)
                    for (int j = 0; j < n; j++)
                        rho[j * totalIntervals + i]
                            = rhoSamplingChangeTimes.contains(times[i]) ?
                                rhos[
                                    constantRho ?
                                    j
                                    : j * (1 + rhoChanges)
                                        + rhoSamplingChangeTimes.indexOf(times[i])
                                ]
                                : 0.;
                computeRhoTips();
            }
        }
        else {
            rho = new Double[n*totalIntervals];
            Arrays.fill(rho, 0.);
        }
    }

    abstract void computeRhoTips();

    /**
     * Collects all the times of parameter value changes and rho-sampling
     * events.
     *
     * @param maxTime
     */
    void collectTimes(double maxTime)
    {
        timesSet.clear();

        getChangeTimes(
            maxTime,
            migChangeTimes,
            migChangeTimesInput.get() != null ?
                migChangeTimesInput.get()
                : intervalTimes.get(),
            migChanges,
            migTimesRelative,
            reverseTimeArrays[5]
        );

        getChangeTimes(
            maxTime,
            birthRateChangeTimes,
            birthRateChangeTimesInput.get() != null ?
                birthRateChangeTimesInput.get()
                : intervalTimes.get(),
            birthChanges,
            birthRateTimesRelative,
            reverseTimeArrays[0]
        );

        getChangeTimes(
            maxTime,
            b_ijChangeTimes,
            b_ijChangeTimesInput.get() != null ?
                b_ijChangeTimesInput.get()
                : intervalTimes.get(),
            b_ij_Changes,
            b_ijTimesRelative,
            reverseTimeArrays[0]
        );

        getChangeTimes(
            maxTime,
            deathRateChangeTimes,
            deathRateChangeTimesInput.get() != null ?
                deathRateChangeTimesInput.get()
                : intervalTimes.get(),
            deathChanges,
            deathRateTimesRelative,
            reverseTimeArrays[1]
        );

        getChangeTimes(
            maxTime,
            samplingRateChangeTimes,
            samplingRateChangeTimesInput.get() != null ?
                samplingRateChangeTimesInput.get()
                : intervalTimes.get(),
            samplingChanges,
            samplingRateTimesRelative,
            reverseTimeArrays[2]
        );

        getChangeTimes(
            maxTime,
            rhoSamplingChangeTimes,
            rhoSamplingTimes.get()!=null ?
                rhoSamplingTimes.get()
                : intervalTimes.get(),
            rhoChanges,
            false,
            reverseTimeArrays[3]
        );

        if (SAModel)
            getChangeTimes(
                maxTime,
                rChangeTimes,
                removalProbabilityChangeTimesInput.get() != null ?
                    removalProbabilityChangeTimesInput.get()
                    : intervalTimes.get(),
                rChanges,
                rTimesRelative,
                reverseTimeArrays[4]
            );

        for (Double time : migChangeTimes)
            timesSet.add(time);

        for (Double time : birthRateChangeTimes)
            timesSet.add(time);

        for (Double time : b_ijChangeTimes)
            timesSet.add(time);

        for (Double time : deathRateChangeTimes)
            timesSet.add(time);

        for (Double time : samplingRateChangeTimes)
            timesSet.add(time);

        for (Double time : rhoSamplingChangeTimes)
            timesSet.add(time);

        if (SAModel)
            for (Double time : rChangeTimes)
                timesSet.add(time);

        times = timesSet.toArray(new Double[timesSet.size()]);
        totalIntervals = times.length;
    }

    /**
     * Returns the change times.
     *
     * @param maxTime
     * @param changeTimes
     * @param intervalTimes
     * @param numChanges
     * @param relative
     * @param reverse
     */
    public void getChangeTimes(
        double maxTime,
        List<Double> changeTimes,
        RealParameter intervalTimes,
        int numChanges,
        boolean relative,
        boolean reverse
    ) {
        changeTimes.clear();

        if (intervalTimes == null) {
            //equidistant
            double intervalWidth = maxTime / (numChanges + 1);
            double end;
            for (int i = 1; i <= numChanges; i++)
            {
                end = (intervalWidth) * i;
                changeTimes.add(end);
            }
            end = maxTime;
            changeTimes.add(end);
        }
        else {
            if (!reverse && intervalTimes.getValue(0) != 0.0)
                throw new RuntimeException(
                    "First time in interval times parameter should "
                    + "always be zero.");

            if (numChanges > 0 && intervalTimes.getDimension() != numChanges + 1)
                throw new RuntimeException(
                    "The time interval parameter should be "
                    + "numChanges + 1 long (" + (numChanges + 1) + ").");

            int dim = intervalTimes.getDimension();

            double end;
            for (int i = (reverse ? 0 : 1); i < dim; i++) {
                end = reverse ?
                        (maxTime - intervalTimes.getValue(dim - i - 1))
                        : intervalTimes.getValue(i);
                if (relative)
                    end *= maxTime;
                if (end < maxTime)
                    changeTimes.add(end);
            }

            if (adjustTimesInput.get() != null) {
                double iTime;
                double aTime = adjustTimesInput.get().getValue();

                for (int i = 0; i < numChanges; i++) {
                    iTime = intervalTimes.getArrayValue(i + 1);
                    if (aTime < iTime) {
                        end = iTime - aTime;
                        if (changeTimes.size() > i)
                            changeTimes.set(i, end);
                        else
                            if (end < maxTime)
                                changeTimes.add(end);
                    }
                }
            }
            end = maxTime;
            changeTimes.add(end);
        }
    }

    /**... */
    void updateBirthDeathPsiParams()
    {
        Double[] birthRates = birthRate.get().getValues();
        Double[] deathRates = deathRate.get().getValues();
        Double[] samplingRates;

        if (samplingCoupledToRemoval)
            // psi will be samplingProportions.
            samplingRates = samplingProportion.get().getValues();
        else
            // psi will be samplingRates.
            samplingRates = samplingRate.get().getValues();
        Double[] removalProbabilities = new Double[1];

        if (SAModel) {
            removalProbabilities = removalProbability.get().getValues();
            r =  new Double[n * totalIntervals];
        }

        int state;

        for (int i = 0; i < n * totalIntervals; i++) {
            state = i / totalIntervals;
            birth[i] =
                identicalRatesForAllTypes[0] ?
                    birthRates[index(times[i % totalIntervals], birthRateChangeTimes)]
                    : birthRates[
                        birthRates.length > n ?
                            (birthChanges+1) * state
                                + index(times[i % totalIntervals], birthRateChangeTimes)
                            : state
                    ];
            death[i] =
                identicalRatesForAllTypes[1] ?
                    deathRates[index(times[i % totalIntervals], deathRateChangeTimes)]
                    : deathRates[
                        deathRates.length > n ?
                            (deathChanges+1) * state
                                + index(times[i % totalIntervals], deathRateChangeTimes)
                            : state
                    ];
            psi[i] =
                identicalRatesForAllTypes[2] ?
                    samplingRates[index(times[i % totalIntervals], samplingRateChangeTimes)]
                    : samplingRates[
                        samplingRates.length > n ?
                            (samplingChanges + 1) * state
                                + index(times[i % totalIntervals], samplingRateChangeTimes)
                            : state
                    ];
            if (SAModel)
                r[i] =
                    identicalRatesForAllTypes[4] ?
                        removalProbabilities[index(times[i % totalIntervals], rChangeTimes)]
                        : removalProbabilities[
                            removalProbabilities.length > n ?
                                (rChanges + 1) * state
                                    + index(times[i % totalIntervals], rChangeTimes)
                                : state
                        ];
        }
    }

    /**
     * ...
     *
     * @param param
     * @param paramFrom
     * @param nrChanges
     * @param changeTimes
     */
    void updateAmongParameter(
        Double[] param,
        Double[] paramFrom,
        int nrChanges,
        List<Double> changeTimes
    ) {
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                for (int dt = 0; dt < totalIntervals; dt++)
                    if (i != j)
                        param[(i*(n-1) + (j<i ? j : j-1)) * totalIntervals + dt]
                            = paramFrom[
                                (paramFrom.length > (n*(n-1))) ?
                                    (nrChanges + 1) * (n-1) * i
                                        + index(times[dt], changeTimes)
                                    : (i*(n-1) + (j<i ? j : j-1))
                            ];
    }

    /**
     * Same as updateAmongParameter, but uses maxState instead of n
     * since number of states might be variable across sites.
     *
     * @param param
     * @param paramFrom
     * @param nrChanges
     * @param changeTimes
     */
    void updateAmongMigrationMatrix(
        Double[] param,
        Double[] paramFrom,
        int nrChanges,
        List<Double> changeTimes
    ) {
        for (int i = 0; i < maxState; i++)
            for (int j = 0; j < maxState; j++)
                for (int dt = 0; dt < totalIntervals; dt++)
                    if (i != j)
                        param[
                            (i * (maxState - 1)
                            + (j<i ? j : j-1)) * totalIntervals
                            + dt
                        ] = paramFrom[
                            (paramFrom.length > (maxState * (maxState - 1))) ?
                                (nrChanges + 1) * (maxState - 1) * i
                                    + index(times[dt], changeTimes)
                                : (i * (maxState - 1) + (j<i ? j : j-1))
                        ];
    }

    /** ... */
    void updateRho() {
        if (
            m_rho.get() != null
            && (
                m_rho.get().getDimension() == 1
                ||  rhoSamplingTimes.get() != null
            )
        ) {
            Double[] rhos = m_rho.get().getValues();
            rho = new Double[n * totalIntervals];
            int state;

            for (int i = 0; i < totalIntervals * n; i++) {
                state  = i / totalIntervals;
                rho[i] = rhoChanges > 0 ?
                            rhoSamplingChangeTimes.contains(times[i]) ?
                                rhos[
                                    rhos.length > n ?
                                        (rhoChanges+1) * state
                                            + index(
                                                times[i % totalIntervals],
                                                rhoSamplingChangeTimes
                                            )
                                        : state
                                ]
                                : 0.
                            : rhos[0];
            }
        }
    }

    /**
     * Returns the nearest index in a list of times corresponding to given
     * time value.
     * <p>
     * This index function should only be used in {@link #transformParameters}.
     * For likelihood calculations the times list needs to be used, e.g. with
     * <code>Untils.index(...)</code>.
     *
     * @param t the time in question
     * @return the index of the given time in the list of times, or if the
     *         time is not in the list, the index of the next smallest time
     */
    public int index(double t, List<Double> times) {
        int epoch = Collections.binarySearch(times, t);
        if (epoch < 0)
            epoch = -epoch - 1;
        return epoch;
    }

    /** ... */
    public void transformWithinParameters() {
        Double[] p = samplingProportion.get().getValues();
        Double[] ds = becomeUninfectiousRate.get().getValues();
        Double[] R;
        if (R0.get() != null)
            R = R0.get().getValues();
        else {
            Double[] l_ratio = lambda_ratio.get().getValues();
            Double[] R_sens = R0_base.get().getValues();

            int totalIntervals = R_sens.length;
            int totalTypes = l_ratio.length + 1;
            R = new Double[totalIntervals * totalTypes];
            for (int i = 0; i < totalIntervals; i++) {
                R[i] = R_sens[i];
                for (int j = 1; j < totalTypes; j++) {
                    double lambda
                        = R_sens[i] * ds[
                            ds.length > totalTypes ?
                            index(times[i%totalIntervals], deathRateChangeTimes)
                            : 0
                        ];
                    R[i + totalIntervals * j]
                        = (lambda * l_ratio[j - 1])
                            /
                            ds[
                                ds.length > totalTypes ?
                                    (deathChanges + 1) * j
                                        + index(times[i % totalIntervals], deathRateChangeTimes)
                                    : j
                            ];
                }
            }
        }

        Double[] removalProbabilities = new Double[1];
        if (SAModel)
            removalProbabilities = removalProbability.get().getValues();

        int state;
        for (int i = 0; i < totalIntervals*n; i++) {
            state =  i / totalIntervals;

            birth[i] =
                (identicalRatesForAllTypes[0] ?
                    R[index(times[i % totalIntervals], birthRateChangeTimes)]
                    : R[
                        R.length > n ?
                            (birthChanges + 1) * state
                                + index(times[i % totalIntervals], birthRateChangeTimes)
                        : state
                    ]
                )
                *
                (identicalRatesForAllTypes[1] ?
                    ds[index(times[i % totalIntervals], deathRateChangeTimes)]
                    : ds[
                        ds.length > n ?
                            (deathChanges + 1) * state
                                + index(times[i % totalIntervals], deathRateChangeTimes)
                            : state
                    ]
                );

            if (!SAModel) {
                psi[i] =
                    (identicalRatesForAllTypes[2] ?
                        p[index(times[i % totalIntervals], samplingRateChangeTimes)]
                        : p[
                            p.length > n ?
                                (samplingChanges + 1) * state
                                    + index(times[i % totalIntervals], samplingRateChangeTimes)
                                : state
                       ]
                    )
                    *
                    (identicalRatesForAllTypes[1] ?
                        ds[index(times[i % totalIntervals], deathRateChangeTimes)]
                        : ds[
                            ds.length > n ?
                                (deathChanges+1) * state
                                    + index(times[i % totalIntervals], deathRateChangeTimes)
                                : state
                        ]
                    );

                death[i] =
                    (identicalRatesForAllTypes[1] ?
                        ds[index(times[i%totalIntervals], deathRateChangeTimes)]
                        : ds[
                            ds.length > n ?
                                (deathChanges+1) * state
                                    + index(times[i%totalIntervals], deathRateChangeTimes)
                                : state
                        ]
                    )
                    - psi[i];
            }
            else {
                r[i] =
                    identicalRatesForAllTypes[4] ?
                        removalProbabilities[index(times[i % totalIntervals], rChangeTimes)]
                        : removalProbabilities[
                            removalProbabilities.length > n ?
                                (rChanges + 1) * state
                                    + index(times[i % totalIntervals], rChangeTimes)
                                : state
                        ];

                psi[i] =
                    (identicalRatesForAllTypes[2] ?
                        p[index(times[i % totalIntervals], samplingRateChangeTimes)]
                        : p[
                            p.length > n ?
                                (samplingChanges + 1) * state
                                    + index(times[i % totalIntervals], samplingRateChangeTimes)
                                : state
                        ]
                    )
                    *
                    (identicalRatesForAllTypes[1] ?
                        ds[index(times[i % totalIntervals], deathRateChangeTimes)]
                        : ds[
                            ds.length > n ?
                                (deathChanges + 1) * state
                                    + index(times[i % totalIntervals], deathRateChangeTimes)
                                : state
                        ]
                    )
                    /
                    (
                        1
                        +
                        (r[i] - 1)
                        *
                        (identicalRatesForAllTypes[2] ?
                            p[index(times[i % totalIntervals], samplingRateChangeTimes)]
                            : p[
                                p.length > n ?
                                    (samplingChanges + 1) * state
                                        + index(times[i%totalIntervals], samplingRateChangeTimes)
                                    : state
                            ]
                        )
                    );

                death[i] =
                    (
                        identicalRatesForAllTypes[1] ?
                        ds[index(times[i % totalIntervals], deathRateChangeTimes)]
                        : ds[
                            ds.length > n ?
                                (deathChanges + 1) * state
                                    + index(times[i % totalIntervals], deathRateChangeTimes)
                                : state
                        ]
                    )
                    -
                    psi[i] * r[i];
            }
        }
    }


    /** ... */
    public void transformAmongParameters() {

        Double[] RaD =
            (birthAmongDemes) ?
                R0AmongDemes.get().getValues()
                : new Double[1];

        Double[] ds = becomeUninfectiousRate.get().getValues();

        if (birthAmongDemes)
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n ; j++)
                    for (int dt = 0; dt < totalIntervals; dt++)
                        if (i != j)
                            b_ij[(i*(n-1) + (j<i ? j : j-1)) * totalIntervals + dt]
                                = RaD[
                                    (RaD.length > ( n*(n-1))) ?
                                        (b_ij_Changes + 1) * (n - 1) * i
                                            + index(times[dt], b_ijChangeTimes)
                                        : (i*(n-1) + (j<i ? j : j-1))
                                ]
                                *
                                ds[
                                    ds.length > n ?
                                        (deathChanges + 1) * i
                                            + index(times[dt], deathRateChangeTimes)
                                        : i
                                ];
    }

    /** ... */
    void checkOrigin(TreeInterface tree) {
        if (origin.get() == null)
            T = tree.getRoot().getHeight();
        else {
            updateOrigin(tree.getRoot());
            if (!Boolean.valueOf(System.getProperty("beast.resume")) && orig < 0)
                throw new RuntimeException(
                    "Error: origin(" + T + ") must be larger than "
                    + "tree height(" + tree.getRoot().getHeight() + ")!");
        }
    }

    /** ... */
    void updateOrigin(Node root) {
        T = origin.get().getValue();
        orig = T - root.getHeight();
        if (originIsRootEdge.get()) {
            orig = origin.get().getValue();
            T = orig + root.getHeight();
        }
    }

    /** Sets up ODEs and integrators. */
    void setupIntegrators() {
        if (minstep == null)
            minstep = T * 1e-100;
        if (maxstep == null)
            maxstep = T / 10;

        Boolean augmented = this instanceof FitnessBirthDeathModel;

        // Set up integrators to solve in dimension of p0, not g equations.
        int fitStates = fitnessModel.getFitSpaceStates();
        Double[] fitValues = fitnessModel.getFitnessSpaceValues();
        Double[] gammas = fitnessModel.getFitnessSpaceGammas();

        // Need to pass fitValues into p0_ODE here!
        P = new p0_ODE(
            birth,
            (birthAmongDemes) ? b_ij : null,
            death,
            psi,
            gammas,
            fitStates,
            fitValues,
            totalIntervals,
            times,
            samplingCoupledToRemoval);
            // P integrator operates only in fitness space.

        PG = new p0ge_ODE(
            birth,
            (birthAmongDemes) ? b_ij : null,
            death,
            psi,
            M,
            fitStates,
            totalIntervals,
            T,
            times,
            P,
            maxEvaluations.get(),
            augmented);
            // Set up for fitness space, not seq space.

        p0ge_ODE.globalPrecisionThreshold = globalPrecisionThreshold;

        if (!useRKInput.get()) {
            pg_integrator = new DormandPrince54Integrator(
                minstep,
                maxstep,
                absoluteTolerance.get(),
                relativeTolerance.get()
            );
            pg_integrator.setMaxEvaluations(maxEvaluations.get());
            PG.p_integrator = new DormandPrince54Integrator(
                minstep,
                maxstep,
                absoluteTolerance.get(),
                relativeTolerance.get()
            );
            PG.p_integrator.setMaxEvaluations(maxEvaluations.get());
        }
        else {
            pg_integrator   = new ClassicalRungeKuttaIntegrator(T / 1000);
            PG.p_integrator = new ClassicalRungeKuttaIntegrator(T / 1000);
        }
    }

    /** Always returns <code>null</code>. */
    @Override
    public List<String> getArguments() {
        return null;
    }

    /** Always returns <code>null</code>. */
    @Override
    public List<String> getConditions() {
        return null;
    }

    /** No-op. */
    @Override
    public void sample(State state, Random random) {}

    /** Always returns <code>true</code>. */
    @Override
    public boolean requiresRecalculation() {
        return true;
    }

    /**
     * Computes p0 conditions for a grid of integration times.
     *
     * @return an array of arrays storing the initial conditions values
     */
    public double[][] getConditionsForPAllTimes() {

        // Get all times
        double endTime = T;
        int dtSteps = (int) (T / dtTimeStep) + 1;
        integrationTimes = new Double[dtSteps];

        double time = 0.0;
        int cntr = 0;
        while (time < endTime) {
            integrationTimes[cntr] = time;
            time += dtTimeStep;
            cntr++;
        }
        integrationTimes[dtSteps - 1] = T; // Ensure final time is included.

        pConditions = new double[dtSteps][fitnessModel.getFitSpaceStates()];

        double t = T - integrationTimes[0];
        boolean rhoSampling =  (m_rho.get() != null);

        pConditions[0] = PG.getP(t, rhoSampling, rho);
        double t0 = t;

        for (int i = 1; i < dtSteps; i++) {
            t = T - integrationTimes[i];
            if (Math.abs(t - t0) < globalPrecisionThreshold) {
                t0 = t;
                pConditions[i] = pConditions[i - 1];
            }
            else {
                pConditions[i] = PG.getP(t, pConditions[i - 1], t0, rhoSampling, rho);
                t0 = t;
            }
        }

        return pConditions;
    }

    /** ... */
    protected Double updateRates() {

        birth = new Double[n*totalIntervals];
        death = new Double[n*totalIntervals];
        psi = new Double[n*totalIntervals];
        b_ij = new Double[totalIntervals*(n*(n-1))];
        M = new Double[totalIntervals*(maxState*(maxState-1))];
            // M has dimension n = seqStates.
        if (SAModel)
            r =  new Double[n * totalIntervals];

        if (transform)
            transformParameters();
        else {
            Double[] birthAmongDemesRates = new Double[1];

            if (birthAmongDemes)
                birthAmongDemesRates = birthRateAmongDemes.get().getValues();

            updateBirthDeathPsiParams();

            if (birthAmongDemes)
                updateAmongParameter(
                    b_ij,
                    birthAmongDemesRates,
                    b_ij_Changes,
                    b_ijChangeTimes
                );
        }

        if (migrationMatrix.get() != null) {
            Double[] migRates = migrationMatrix.get().getValues();

            // Added this just for FluGenotypeFitnessModel.
            // Can remove if no longer needed.
            int migRatesSize = maxState * (maxState - 1);
            if (migRates.length != migRatesSize) {
                double mRate = migRates[0];
                migRates = new Double[migRatesSize];
                for (int state = 0; state < migRatesSize; state++)
                    migRates[state] = mRate;
            }

            Double factor;
            if (migrationMatrixScaleFactor.get()!=null) {
                factor = migrationMatrixScaleFactor.get().getValue();
                for (int i = 0; i < migRates.length; i++)
                    migRates[i] *= factor;
            }

            updateAmongMigrationMatrix(M, migRates, migChanges, migChangeTimes);
                // Seems to work even without corrected method.
        }

        updateRho();
        freq = frequencies.get().getValues();
        fitnessModel.update();
        setupIntegrators();

        return 0.;
    }

    /** ... */
    public void transformParameters()
    {
        transformWithinParameters();
        transformAmongParameters();
    }
}
