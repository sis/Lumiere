package lumiere.distribution;

import lumiere.alignment.UnsortedAlignment;
import lumiere.math.SmallNumber;
import lumiere.math.p0ge_SiteConditions;
import lumiere.util.Utils;

import beast.base.core.Loggable;
import beast.base.core.Input;
import beast.base.core.Description;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeInterface;


/**
 * Marginal fitness birth-death model with multiple evolving sites.
 * <p>
 * Based on the original multi-deme birth-death model implemented by Denise
 * Kuehnert, modified for the marginal fitness birth-death model. This version
 * may not be compatible with some of the original features of the MTBD model,
 * like sampled ancestor trees.
 * <p>
 * The implementation uses the {@link SmallNumber} class, designed to prevent
 * numerical underflow at the cost of additional computational complexity.
 *
 * @author Denise Kuehnert (July 2013)
 * @author David Rasmussen (December 2017)
 */

@Description(
"Implements the marginal fitness birth-death model with multiple "
+ "evolving sites. It is based on the original multi-deme birth-death "
+ "model implemented by Denise Kuehnert. "
+ "This version may not be compatible with some of the original features "
+ "of the MTBD model, like sampled ancestor trees. "
+ "This version is implemented to prevent numerical underflowing, "
+ "using so-called 'SmallNumbers', at the cost of additional "
+ "computational complexity")

public class FitnessBirthDeathModel
    extends PiecewiseFitnessBirthDeathDistribution
    implements Loggable
{

    /**
     * Store tip node types?
     * <p>
     * This assumes that tip types cannot change.
     */
    public Input<Boolean> storeNodeTypes = new Input<Boolean>(
        "storeNodeTypes",
        "Store tip node types? This assumes that tip types cannot "
        + "change. (default: false)",
        false
    );

    /** Holds tip sequence data. */
    protected int[][] tipSeqStates;

    private int[] nodeStates;
        // MFBD model currently does not use nodeStates. But these could
        // hold an additional state not given by the sequence data.

    Boolean print = false;

    @Override
    public void initAndValidate()
    {
        TreeInterface tree = treeInput.get();
        checkOrigin(tree);
        ntaxa = tree.getLeafNodeCount();
        super.initAndValidate();

        int contempCount = 0;
        for (Node node : tree.getExternalNodes())
            if (node.getHeight() == 0.0)
                contempCount++;

        // Store tip sequence data from alignment.
        tipSeqStates = new int[ntaxa][siteCount];
        for (Node node : tree.getExternalNodes()) {
            int taxonIndex = getTaxonIndex(node.getID(), seqData);
            for (int i = 0; i < siteCount; i++)
                tipSeqStates[node.getNr()][i] = seqData.getPattern(taxonIndex, i);
                // Ambiguous states are ignored.
        }

        if (checkRho.get() && contempCount > 1 && rho == null)
            throw new RuntimeException(
                "Error: multiple tips given at present, but sampling "
                + "probability 'rho' is not specified.");

        collectTimes(T);
        setRho();
    }

    /**
     * Method copied from
     * <code>beast.base.evolution.likelihood.TreeLikelihood</code>-
     *
     * @param taxon the taxon name as a string
     * @param data the alignment
     * @return the taxon index of the given taxon name for accessing its
     *         sequence data in the given alignment, or -1 if the taxon is not
     *         in the alignment.
     */
    private int getTaxonIndex(String taxon, UnsortedAlignment data) {
        int taxonIndex = data.getTaxonIndex(taxon);
        if (taxonIndex == -1) {
            if (taxon.startsWith("'") || taxon.startsWith("\""))
               taxonIndex = data.getTaxonIndex(taxon.substring(1, taxon.length() - 1));
            if (taxonIndex == -1)
                throw new RuntimeException(
                    "Could not find sequence " + taxon + " in the alignment");
        }
        return taxonIndex;
    }

    /** ... */
    void computeRhoTips()
    {
        for (Node tip : treeInput.get().getExternalNodes())
            isRhoTip[tip.getNr()] = false;
    }

    /**
     * <code>getG</code> implementation for ge equation using
     * {@link SmallNumber} objects to avoid numerical underflowing of
     * integration results.
     */
    public p0ge_SiteConditions getG(double t, p0ge_SiteConditions PG0, double t0, Node node)
    {
        // PG0 contains initial condition for p0 (0..n-1) and for ge (n..2n-1).
        if (node.isLeaf())
            // pInitialConditions are no longer indexed by node numbers,
            // but by times. But these initial conditions are never used
            // anyways, so set to time 0.
            System.arraycopy(
                pInitialConditions[0],
                0,
                PG0.conditionsOnP,
                0,
                fitnessModel.getFitSpaceStates()
            );
        return getGFastEuler(t, PG0, t0, pg_integrator, T, maxEvalsUsed);
    }

    /**
     * Returns the log-likelihood of a given tree for the current set of
     * parameters.
     * <p>
     * [following warning may be outdated]
     * WARNING: calculateTreeLogLikelihood allows use of both classic and
     * non-underflowing methods. Some chunks of code are therefore present
     * in two similar versions in this method.
     * When modifying one of the versions, one should check if the other
     * version also needs the corresponding changes.
     *
     * @param tree phylogenetic tree
     * @return log-likelihood of given tree and parameter set
     */
    @Override
    public double calculateTreeLogLikelihood(TreeInterface tree) {

        Node root = tree.getRoot();

        if (origin.get() == null)
            T = root.getHeight();
        else
            updateOrigin(root);

        collectTimes(T);
        setRho();

        if (
            (orig < 0)
            || updateRates() < 0
            || (times[totalIntervals - 1] > T)
        ) {
            logP = Double.NEGATIVE_INFINITY;
            return logP;
        }

        double[] noSampleExistsProp = new double[]{0.0};
        SmallNumber[] PrSN = new SmallNumber[siteCount]; // PrSN for each site
        SmallNumber full_tree_pD = new SmallNumber(0.0); // pD for entire tree
        for (int k = 0; k < siteCount; k++)
            PrSN[k] = new SmallNumber(0.0);

        try
        {
            // Start calculation.
            // Compute P0 conditions at all time points.
            pInitialConditions = getConditionsForPAllTimes();

            if (conditionOnSurvival.get())
            {

                // noSampleExistsProp = pInitialConditions[pInitialConditions.length-1];
                // [ceci] pE at the root as in matlab code

                int tx = 0;
                for (int idx = 0; idx < integrationTimes.length; ++idx)
                    if (integrationTimes[idx] <= root.getHeight())
                        tx = idx;

                noSampleExistsProp = pInitialConditions[tx];
                if (print)
                    System.out.println(
                        "\nnoSampleExistsProp = "
                        + noSampleExistsProp[0]
                        + ", "
                        + noSampleExistsProp[1]
                    );
            }

            p0ge_SiteConditions pSN = new p0ge_SiteConditions();

            // [ceci] Temporal change to agree with matlab code
            // if (orig > 0)
            if (orig >= 0)
                // If origin is before root.
                pSN = calculateSubtreeLikelihood(root,0, orig);
            else {
                int childIndex = 0;
                if (root.getChild(1).getNr() > root.getChild(0).getNr()) {
                    childIndex = 1;
                    // Always start with the same child to avoid
                    // numerical differences.
                }

                pSN = calculateSubtreeLikelihood(
                    root.getChild(childIndex),
                    0.,
                    T - root.getChild(childIndex).getHeight()
                );
                childIndex = Math.abs(childIndex-1);

                p0ge_SiteConditions p1SN = calculateSubtreeLikelihood(
                    root.getChild(childIndex),
                    0.,
                    T - root.getChild(childIndex).getHeight()
                );

                for (int k = 0; k < siteCount; k++) {
                    // Now over all sites.
                    for (int i = 0; i < seqStates[k]; i++)
                        pSN.conditionsOnG[k][i] =
                            SmallNumber.multiply(
                                pSN.conditionsOnG[k][i],
                                p1SN.conditionsOnG[k][i]
                            );
                }
            }

            if (print)
                System.out.print("final p per state = ");

            // Compute final probability density at root for each site.
            double[][] marginalFitEffects =
                fitnessModel.getMarginalFitnessEffectsSN(pSN.conditionsOnG);

            // Only need these if approxEProbs == false.
            double[][] marginalSiteProbs = getMarginalSiteProbsSN(pSN.conditionsOnG);
            int[][] genotypes = fitnessModel.getGenotypes();

            for (int k = 0; k < siteCount; k++) {
                for (int rootState = 0; rootState < seqStates[k]; rootState++) {
                    if (pSN.conditionsOnG[k][rootState].getMantissa() > 0) {
                        if (!conditionOnSurvival.get())
                            // PrSN[k] += pSN.conditionsOnG[k][rootState] * freq[rootState]
                            PrSN[k] =
                                SmallNumber.add(
                                    PrSN[k],
                                    pSN.conditionsOnG[k][rootState]
                                       .scalarMultiply(freq[rootState])
                                );
                        else {
                            double expectedP0;
                            if (approxEProbs) {
                                int fitClass =
                                    fitnessModel.getClosestFitSpaceState(
                                        marginalFitEffects[k][rootState]
                                    );
                                expectedP0 = noSampleExistsProp[fitClass];
                            }
                            else {
                                expectedP0 = 0;
                                double[] gpr = new double[genotypes.length];
                                double gprSum = 0;
                                for (int geno = 0; geno < genotypes.length; geno++)
                                    if (genotypes[geno][k] == rootState) {
                                        // Otherwise no probability of being
                                        // in this genotype.
                                        gpr[geno] = 1.0;
                                        for (int j = 0; j < siteCount; j++)
                                        {
                                            if (k != j)
                                            {
                                                // Already accounted for this
                                                // site above.
                                                int type = genotypes[geno][j];
                                                gpr[geno] *= marginalSiteProbs[j][type];
                                            }
                                        }
                                        gprSum += gpr[geno];
                                    }

                                if (gprSum <= 0)
                                    for (int geno = 0; geno < genotypes.length; geno++)
                                        expectedP0 +=
                                            noSampleExistsProp[geno]
                                            /
                                            genotypes.length;
                                else
                                    for (int geno = 0; geno < genotypes.length; geno++)
                                        expectedP0 +=
                                            noSampleExistsProp[geno]
                                            *
                                            gpr[geno]
                                            /
                                            gprSum;

                            }
                            // See equation (21) in Lumière paper.
                            PrSN[k] =
                                SmallNumber.add(
                                    PrSN[k],
                                    pSN.conditionsOnG[k][rootState]
                                       .scalarMultiply(freq[rootState])
                                       .scalarMultiply(1/(1-expectedP0)));
                        }
                    }
                }
            }

            // Should full_tree_pD be conditioned on survival as well?
            if (!conditionOnSurvival.get())
                full_tree_pD = pSN.conditionsOnG[siteCount][0];
            else {
                // [ceci] condition on root as in  matlab code
                // marginalFitEffects[siteCount][0] stores the product of
                // weighted fitness effects.
                int fitClass =
                    fitnessModel.getClosestFitSpaceState(
                        marginalFitEffects[siteCount][0]
                    );
                full_tree_pD =
                    pSN.conditionsOnG[siteCount][0]
                        .scalarMultiply(1 / (1 - noSampleExistsProp[fitClass]));
            }

        }
        catch(Exception e) {
            if (e instanceof ConstraintViolatedException)
                throw e;
            logP = Double.NEGATIVE_INFINITY;
            return logP;
        }

        maxEvalsUsed = Math.max(maxEvalsUsed, PG.maxEvalsUsed);

        // See equations (22) and (23) in Lumière paper.
        // Need take product over all sites and divide by total tree density.
        logP = 0;
        for (int k = 0; k < siteCount; k++)
            logP += PrSN[k].log();
        // "Divide" by full probability of tree.
        logP -= (siteCount-1) * full_tree_pD.log();

        if (print)
            System.out.println("\nlogP = " + logP);

        if (Double.isInfinite(logP))
            logP = Double.NEGATIVE_INFINITY;

        if (
            SAModel
            && !(removalProbability.get().getDimension() == n
            && removalProbability.get().getValue() == 1.0)
        ) {
            int internalNodeCount =
                tree.getLeafNodeCount()
                - ((Tree)tree).getDirectAncestorNodeCount()- 1;
            logP +=  Math.log(2)*internalNodeCount;
        }

        return logP;
    }

    /**
     * Calculates the likelihood of the partial tree underneath a given node.
     * <p>
     * Avoids underflowing of integration results by using the {@link
     * SmallNumber} class.
     *
     * @param node the node at the root of the subtree
     * @param from point in time to integrate from
     * @param to   point in time to integrate to
     */
    p0ge_SiteConditions calculateSubtreeLikelihood(
        Node node,
        double from,
        double to
    ) {

        double[] pconditions = new double[fitnessModel.getFitSpaceStates()];
            // E probability of no sampled descendants.
        SmallNumber[][] gconditions = new SmallNumber[siteCount+1][];
            // Now holds gconditions for all sites.

        for (int k = 0; k < siteCount; k++)
        {
            gconditions[k] = new SmallNumber[seqStates[k]];
            for (int i = 0; i < seqStates[k]; i++)
            {
                gconditions[k][i] = new SmallNumber(0.0);
            }
        }

        gconditions[siteCount] = new SmallNumber[1];
            // Final "site" holds gconditions for full tree.

        gconditions[siteCount][0] = new SmallNumber(0.0);
        p0ge_SiteConditions init  = new p0ge_SiteConditions(pconditions, gconditions);
            // With gconditions for all sites.

        int index = Utils.index(to,times, totalIntervals);
            // time interval index for parameters with change points

        if (node.isLeaf()) {

            // sampling event
            int nodestate;

            if (!isRhoTip[node.getNr()]) {
                for (int k = 0; k < siteCount; k++) {
                    nodestate = tipSeqStates[node.getNr()][k];
                    if (samplingCoupledToRemoval)
                        init.conditionsOnG[k][nodestate] =
                            new SmallNumber(death[index] * psi[index]);
                            // removal with sampling
                    else
                        init.conditionsOnG[k][nodestate] =
                            SAModel ?
                            new SmallNumber(
                                (r[index]
                                    + pInitialConditions[node.getNr()][nodestate]
                                        * (1 - r[index])
                                )
                                    // Handling of sampling probabilities under
                                    // SAModel unchecked here and probably not
                                    // correct.
                                * psi[index])
                                    // With SA: ψ_i(r + (1 − r)p_i(τ))
                            : new SmallNumber(psi[index]);
                                // psi is the sampling rate.
                }

                // Need to include sampling probabilities for full_tree_pD.
                if (samplingCoupledToRemoval)
                    init.conditionsOnG[siteCount][0] =
                        new SmallNumber(death[index] * psi[index]);
                else
                    init.conditionsOnG[siteCount][0] = new SmallNumber(psi[index]);
                    // psi is the sampling rate.

            }
            else {
                for (int k = 0; k < siteCount; k++) {
                    nodestate = tipSeqStates[node.getNr()][k];
                    init.conditionsOnG[k][nodestate] =
                        SAModel ?
                        new SmallNumber(
                            (r[index]
                                + pInitialConditions[node.getNr()][nodestate]
                                / (1 - rho[index])
                                * (1-r[index])
                            ) * rho[index]
                        )
                        : new SmallNumber(rho[index]);
                            // rho-sampled leaf in the past:
                            // ρ_i(τ)(r + (1 − r)p_i(τ+δ))
                            // The +δ is translated by dividing p_i with 1-ρ_i
                            // (otherwise there's one too many "*ρ_i").

                }
                // Need to include sampling probabilities for full_tree_pD.
                init.conditionsOnG[siteCount][0] = new SmallNumber(rho[index]);
                    // rho is the sampling fraction.
            }

            if (print)
                System.out.println("Sampling at time " + (T-to));

            return getG(from, init, to, node);

        }
        else if (node.getChildCount() == 2) {

            // birth / infection event or sampled ancestor
            if (
                node.getChild(0).isDirectAncestor()
                || node.getChild(1).isDirectAncestor()
            ) {
                // Found a sampled ancestor.
                throw new ConstraintViolatedException(
                    "Error: Sampled ancestors not implemented "
                    + "in multi-site fitness models.");
            }
            else {
                // birth / infection event
                int childIndex = 0;
                if (node.getChild(1).getNr() > node.getChild(0).getNr())
                    childIndex = 1;
                    // Always start with the same child to avoid
                    // numerical differences.

                p0ge_SiteConditions g0 =
                    calculateSubtreeLikelihood(
                        node.getChild(childIndex),
                        to,
                        T - node.getChild(childIndex).getHeight()
                    );
                childIndex = Math.abs(childIndex - 1);
                p0ge_SiteConditions g1 =
                    calculateSubtreeLikelihood(
                        node.getChild(childIndex),
                        to,
                        T - node.getChild(childIndex).getHeight()
                    );

                if (print)
                    System.out.println("Infection at time " + (T - to));

                // Need to compute marginal fitness for each child lineage
                // from SmallNumbers.
                double[][] marginalFitEffectsChild0 =
                    fitnessModel.getMarginalFitnessEffectsSN(g0.conditionsOnG);
                double[][] marginalFitEffectsChild1 =
                    fitnessModel.getMarginalFitnessEffectsSN(g1.conditionsOnG);

                // Don't forget to copy over init.conditionsOnP.
                System.arraycopy(
                    g0.conditionsOnP,
                    0,
                    init.conditionsOnP,
                    0,
                    fitnessModel.getFitSpaceStates());
                    // No longer necessary.

                for (int k = 0; k < siteCount; k++) {
                    for (int i = 0; i < seqStates[k]; i++) {
                        final double child0Lambda =
                            birth[index] * marginalFitEffectsChild0[k][i];
                        final double child1Lambda =
                            birth[index] * marginalFitEffectsChild1[k][i];
                        init.conditionsOnG[k][i] =
                            SmallNumber.multiply(
                                g0.conditionsOnG[k][i],
                                g1.conditionsOnG[k][i]
                            )
                            .scalarMultiply(child0Lambda + child1Lambda);
                        // Mutation can't occur at birth event so both child
                        // lineages need to be in state i BUT either child
                        // could have been parent.
                    }
                }

                // Update G for full_tree_pD.
                final double child0Lambda =
                    birth[index] * marginalFitEffectsChild0[siteCount][0];
                final double child1Lambda =
                    birth[index] * marginalFitEffectsChild1[siteCount][0];
                init.conditionsOnG[siteCount][0] =
                    SmallNumber.multiply(
                        g0.conditionsOnG[siteCount][0],
                        g1.conditionsOnG[siteCount][0]
                    )
                    .scalarMultiply(child0Lambda + child1Lambda);
            }
        }
        else
            // Found a single child node.
            throw new RuntimeException(
                "Error: Single child-nodes found (although not using "
                + "sampled ancestors)");

        if (print)
        {
            System.out.print("p after subtree merge = ");
            for (int i = 0; i < n;i++)
                System.out.print(init.conditionsOnP[i] + "\t");
            for (int i = 0; i < n; i++)
                System.out.print(init.conditionsOnG[i] + "\t");
            System.out.println();
        }

        return getG(from, init, to, node);
    }

    /** Error thrown when state assignment went wrong. */
    protected class ConstraintViolatedException extends RuntimeException
    {
        private static final long serialVersionUID = 1L;

        public ConstraintViolatedException(String s) {
            super(s);
        }
    }

}
