package lumiere.math;

import lumiere.util.Utils;

import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;

import java.util.Arrays;


/**
 * ...
 *
 * @author Denise Kuehnert (July 2013)
 */

public class p0ge_ODE implements FirstOrderDifferentialEquations {

    p0_ODE P;
    public FirstOrderIntegrator p_integrator;

    Double[] b;
    Double[] b_ij;
    Double[] d;
    Double[] s;

    Boolean augmented;
    Boolean birthAmongDemes;

    Double[] M;
    double T;

    int dimension;  /* ODE dimension = stateNumber */
    int intervals;
    Double[] times;
    int index;

    int maxEvals;
    public int maxEvalsUsed;
    public static double globalPrecisionThreshold;

    /** ... */
    public p0ge_ODE(
        Double[] b,
        Double[] b_ij,
        Double[] d,
        Double[] s,
        Double[] M,
        int dimension,
        int intervals,
        double T,
        Double[] times,
        p0_ODE P,
        int maxEvals,
        Boolean augmented
    ) {
        this.b = b;
        this.b_ij = b_ij;
        this.d = d;
        this.s = s;
        this.M = M;
        this.dimension = dimension;
        this.intervals = intervals;
        this.maxEvals = maxEvals;
        maxEvalsUsed = 0;
        this.T = T;
        this.times= times;
        this.P = P;
        this.augmented = augmented;
        this.birthAmongDemes = b_ij!=null;
    }

    /** Returns the <code>dimension</code> times 2. */
    public int getDimension() {
        return 2*this.dimension;
    }

    /** ... */
    // Are these used? If yes, they should reflect model in fitness space
    // for p0.
    public void computeDerivatives(double t, double[] g, double[] gDot) {

        index = Utils.index(t, times, intervals);

        int k, l;

        for (int i=0; i<dimension; i++){

            /*  p0 equations (0 .. dim-1) */

            k = i*intervals + index;

            gDot[i] = + (b[k]+d[k]+s[k]
                    - b[k] * g[i]) * g[i]
                    - d[k] ;

            for (int j=0; j<dimension; j++){
                l = (i*(dimension-1)+(j<i?j:j-1))*intervals + index;
                if (i != j){
                    if (birthAmongDemes){
                        // infection among demes
                        gDot[i] += b_ij[l]*g[i];
                        // b_ij[intervals * i * (dimension-1)
                        //      + (j<i ? j : j-1) + index] * g[i];
                        gDot[i] -= b_ij[l]*g[i]*g[j];
                        // b_ij[intervals * i * (dimension-1)
                        //      + (j < i ? j : j-1) + index] * g[i] * g[j];
                    }
                    if (M[0]!=null) {
                        // migration
                        gDot[i] += M[l] * g[i];
                        gDot[i] -= M[l] * g[j];
                    }
                }
            }

            /* ge equations: (dim .. 2*dim-1) */

            gDot[dimension+i] = + (b[k]+d[k]+s[k]
                    - 2*b[k]*g[i])*g[dimension+i];

            for (int j=0; j<dimension; j++) {

                l = (i*(dimension-1) + (j<i ? j : j-1)) * intervals + index;
                if (i != j) {
                    if (b_ij != null) {
                        // infection among demes
                        gDot[dimension+i] += b_ij[l]*g[dimension+i];
                        if (!augmented) {
                            gDot[dimension+i] -=
                                b_ij[l]
                                * (g[i]*g[dimension+j] + g[j]*g[dimension+i]);
                        }
                    }

                    if (M[0] != null) {
                        // migration
                        gDot[dimension + i] += M[l] * g[dimension + i];
                        if (!augmented)
                            gDot[dimension + i] -= M[l] * g[dimension + j];
                    }
                }
            }
        }

        // gDot[2] = -(-(b[0]+b_ij[0]+d[0]+s[0])*g[2] + 2*b[0]*g[0]*g[2]
        //             + b_ij[0]*g[0]*g[3] + b_ij[0]*g[1]*g[2]);
        // gDot[3] = -(-(b[1]+b_ij[1]+d[1]+s[1])*g[3] + 2*b[1]*g[1]*g[3]
        //              + b_ij[1]*g[1]*g[2] + b_ij[1]*g[0]*g[3]);

    }

    /** Performs integration on differential equations p. */
    public double[] getP(
        double t,
        double[]P0,
        double t0,
        Boolean rhoSampling,
        Double[] rho
    ) {
        if (Math.abs(T-t) < globalPrecisionThreshold
            || Math.abs(t0-t) < globalPrecisionThreshold
            || T < t
        )
            return P0;

        double[] result = new double[P0.length];

        try {
            System.arraycopy(P0, 0, result, 0, P0.length);

            double from = t;
            double to = t0;
            double oneMinusRho;

            int indexFrom = Utils.index(from, times, times.length);
            int index = Utils.index(to, times, times.length);

            int steps = index - indexFrom;

            index--;

            if (Math.abs(from-times[indexFrom]) < globalPrecisionThreshold)
                steps--;

            if (index > 0 && Math.abs(to-times[index-1]) < globalPrecisionThreshold) {
                steps--;
                index--;
            }

            while (steps > 0) {

                from = times[index];

                // TO DO: Putting the if(rhosampling) in there also means
                // the 1-rho may never be actually used so a workaround
                // is potentially needed.
                if (Math.abs(from-to) > globalPrecisionThreshold){

                    // Solve P , store solution in y.
                    p_integrator.integrate(P, to, result, from, result);

                    if (rhoSampling)
                        for (int i=0; i<dimension; i++) {
                            // oneMinusRho = (1 - rho[i*intervals + index]);
                            oneMinusRho = (1 - rho[index]);
                            result[i] *= oneMinusRho;
                        }
                }

                to = times[index];

                steps--;
                index--;
            }

            // Solve P, store solution in y.
            p_integrator.integrate(P, to, result, t, result);

            // TO DO
            // Check that both times are really overlapping.
            // But really not sure that this is enough, I have to build
            // appropriate tests.
            if (Math.abs(t-times[indexFrom]) < globalPrecisionThreshold)
                if (rhoSampling)
                    for (int i=0; i<dimension; i++) {
                        // oneMinusRho = (1 - rho[i*intervals + indexFrom]);
                        oneMinusRho = (1 - rho[indexFrom]);
                        result[i] *= oneMinusRho;
                    }

        }
        catch(Exception e) {
            throw new RuntimeException("Couldn't calculate p.");
        }

        return result;
    }

    /** ... */
    public double[] getP(double t, Boolean rhoSampling, Double[] rho) {

        // Dimension should equal fitnessModel.getFitSpaceStates().

        double[] y = new double[dimension];

        if (!rhoSampling)
            // initial condition: y_i[T] = 1 for all i
            Arrays.fill(y, 1.);
        else
            for (int i = 0; i < dimension; i++)
                // y[i] = (1 - rho[i * intervals + Utils.index(T, times, intervals)]);
                // initial condition: y_i[T] = 1 - rho_i
                y[i] = (1 - rho[Utils.index(T, times, intervals)]);
                    // Since rho will not be indexed only by the time interval.

        if (Math.abs(T-t) < globalPrecisionThreshold || T < t)
            return y;

        return getP(t, y, T, rhoSampling, rho);

    }

}
