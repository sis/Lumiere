package lumiere.math;

import beast.base.core.Description;


/**
 * Set of initial conditions for a system of ODEs, all increased by the same
 * factor to prevent underflowing.
 * <p>
 * Underflowing  appears with numbers too small to be distinguished from zero
 * when using the type <code>double</code>.
 *
 * @author Jeremie Scire (June 2016)
 */

@Description(
"Set of initial conditions for a system of ODEs, all increased by the "
+ "same factor to prevent underflowing (which appears with numbers "
+ "too small to be distinguished from zero, when using the type 'double')")

public class ScaledNumbers {

    private int factor;
    private double[] equation;

    /**
     * Initializes the object with given values.
     *
     * @param f scale factor
     * @param e set of initial conditions on which the scale factor was applied
     */
    public ScaledNumbers(int f, double[] e) {
        this.factor = f;
        this.equation = e;
    }

    /** Initializes the object with default values: zeros everywhere. */
    public ScaledNumbers() {
        this.factor = 0;
        this.equation = new double[0];
    }

    /** Returns the <code>equation</code>. */
    public double[] getEquation() {
        return this.equation;
    }

    /** Sets the <code>equation</code>. */
    public void setEquation(double[] newEquation) {
        this.equation = newEquation;
    }

    /** Returns the scaling <code>factor</code>. */
    public int getScalingFactor() {
        return this.factor;
    }

    /** Sets the scaling <code>factor</code>. */
    public void setScalingFactor(int newFactor) {
        this.factor = newFactor;
    }

    /** Increases the scaling <code>factor</code>. */
    public void augmentFactor(int increase) {
        factor += increase;
    }
}
