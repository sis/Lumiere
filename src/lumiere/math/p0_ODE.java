package lumiere.math;

import lumiere.util.Utils;

import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;


/**
 * ...
 *
 * @author Denise Kuehnert (May 2012)
 */

public class p0_ODE implements FirstOrderDifferentialEquations {

    Double[] b;
    Double[] b_ij;
    Double[] d;
    Double[] s;

    Double[] M;

    Double[] fitValues;

    int dimension;
    int intervals;
    Double[] times;
    int index;

    Boolean samplingCoupledToRemoval;

    /** ... */
    public p0_ODE(
        Double[] b,
        Double[] b_ij,
        Double[] d,
        Double[] s,
        Double[] M,
        int dimension,
        Double[] fitValues,
        int intervals,
        Double[] times,
        Boolean samplingCoupledToRemoval
    ) {
        this.b = b;
        this.b_ij = b_ij;
        this.d = d;
        this.s = s;
        this.M = M;
        this.dimension = dimension;
        this.fitValues = fitValues;
        this.intervals = intervals;
        this.times = times;
        this.samplingCoupledToRemoval = samplingCoupledToRemoval;
    }

    /** ... */
    public void updateRates(
        Double[] b,
        Double[] b_ij,
        Double[] d,
        Double[] s,
        Double[] M,
        Double[] fitValues,
        Double[] times
    ){
        this.b = b;
        this.b_ij = b_ij;
        this.d = d;
        this.s = s;
        this.M = M;
        this.fitValues = fitValues;
        this.times = times;
    }

    /** Returns the <code>dimension</code>. */
    public int getDimension() {
        return this.dimension;
    }

    /** ... */
    public void computeDerivatives(double t, double[] y, double[] yDot) {

        // Find the index of the time interval t lies in.
        index = Utils.index(t, times, intervals);

        int k, l;
        for (int i = 0; i < dimension; i++){

            k = index;

            double lambda = b[k] * fitValues[i];  // Need to pass in fitValues.

            if (samplingCoupledToRemoval)
                yDot[i] = + (lambda+d[k])*y[i] - (d[k] * (1-s[k])) - lambda*y[i]*y[i];
            else
                yDot[i] = + (lambda+d[k]+s[k])*y[i] - d[k] - lambda*y[i]*y[i];

            for (int j=0; j<dimension; j++) {

                // l = (i*(dimension-1)+(j<i?j:j-1))*intervals + index;
                l = i*(dimension-1) + (j<i ? j : j-1);  // linear indexing

                if (i != j) {

                    // No birthAmongDemes in multi-site fitness model.
                    // if (b_ij!=null) {     // infection among demes
                    //     yDot[i] += b_ij[l]*y[i];
                    //     yDot[i] -= b_ij[l]*y[i]*y[j];
                    // }

                    if (M[0]!=null) {   // migration
                        yDot[i] += M[l] * y[i];
                        yDot[i] -= M[l] * y[j];
                    }
                }
            }
        }

    }

}
