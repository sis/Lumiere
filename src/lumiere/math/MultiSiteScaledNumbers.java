package lumiere.math;

import beast.base.core.Description;


/**
 * Set of initial conditions for a system of ODEs, all increased by the
 * same factor to prevent underflowing.
 * <p>
 * Underflowing occurs with numbers too small to be distinguished from zero
 * when using the type <code>double</code>.
 *
 * @author Jeremie Scire (June 2016)
 */

@Description(
"Set of initial conditions for a system of ODEs, all increased by the "
+ "same factor to prevent underflowing (which appears with numbers "
+ "too small to be distinguished from zero, when using the type 'double')")

public class MultiSiteScaledNumbers {

    private int factor;
    private double[][] equation;
    private double[] p0equation;

    /**
     * Initializes the object with given values.
     *
     * @param f scaling factor
     * @param e set of initial conditions on which the scale factor was applied
     * @param p p0equation
     */
    public MultiSiteScaledNumbers(int f, double[][] e, double[] p) {
        this.factor = f;
        this.equation = e;
        this.p0equation = p;
    }

    /** Initializes the object with default values: zeros everywhere. */
    public MultiSiteScaledNumbers() {
        this.factor = 0;
        this.equation = new double[0][0];
        this.p0equation = new double[0];
    }

    /** Returns <code>equation</code>. */
    public double[][] getEquation() {
        return this.equation;
    }

    /** Returns <code>p0equation</code>. */
    public double[] getP0Equation() {
        return this.p0equation;
    }

    /** Sets <code>equation</code>. */
    public void setEquation(double[][] newEquation) {
        this.equation = newEquation;
    }

    /** Returns the scaling <code>factor</code>. */
    public int getScalingFactor() {
        return this.factor;
    }

    /** Sets the scaling <code>factor</code>. */
    public void setScalingFactor(int newFactor) {
        this.factor = newFactor;
    }

    /** Increases the scaling <code>factor</code>. */
    public void augmentFactor(int increase) {
        factor += increase;
    }
}
