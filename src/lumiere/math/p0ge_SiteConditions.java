package lumiere.math;

import beast.base.core.Description;


/**
 * This type contains both sets of initial conditions for both equation
 * types: p and ge.
 *
 * @author Jeremie Scire
 * @author David Rasmussen (modified to hold g conditions for every site)
 */

@Description(
"This type contains both sets of initial conditions for both "
+ "equation types: p and ge")

public class p0ge_SiteConditions {

    int dimension;

    /** ... */
    public SmallNumber[][] conditionsOnG;

    /** ... */
    public double[] conditionsOnP;

    /** Initializes the object with given values. */
    public p0ge_SiteConditions(double[] pcond, SmallNumber[][] gcond) {

        // This will no longer be true since pcond will be in fitness space
        // and gcond will be in seq space.
        // if (pcond.length != gcond.length) {
        //     throw new RuntimeException(
        //         "Incorrect initialization: difference of size between "
        //         + "conditionsOnG and conditionsOnP");
        // }

        dimension = gcond.length; // Or should this be pcond.length?
        conditionsOnP = pcond;
        conditionsOnG = gcond;
    }

    /** Initializes the object with default values: zeros everywhere. */
    public p0ge_SiteConditions() {
        dimension = 1;
        conditionsOnP = new double[] {0};
        conditionsOnG = new SmallNumber[][] {{new SmallNumber(0.0)}};
            // Proper init for a 2D array?
    }

    /** Returns <code>conditionsOnP</code>. */
    public double[] getConditionsOnP() {
        return this.conditionsOnP;
    }

    /** Returns <code>conditionsOnG</code>. */
    public SmallNumber[][] getConditionsOnG() {
        return this.conditionsOnG;
    }

    /** Returns whether the sum over <code>conditionsOnG</code> for a
        given site is (true) zero. */
    public boolean isSumGZero(int site) {
        SmallNumber sum = new SmallNumber(0.0);
        for (int i = 0; i < dimension; i++)
            sum = SmallNumber.add(sum, conditionsOnG[site][i]);
        double convertedSum = sum.revert();
        return (convertedSum <= 0);
    }

}
