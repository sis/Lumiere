package lumiere.math;

import beast.base.core.Description;


/**
 * This type contains both sets of initial conditions for both equation types:
 * p and ge.
 *
 * @author Jeremie Scire
 */

@Description(
"This type contains both sets of initial conditions for both equation "
+ "types: p and ge")

public class p0ge_InitialConditions {

    int dimension;
    public SmallNumber[] conditionsOnG;
    public double[] conditionsOnP;

    /** Initializes the object with given values. */
    public p0ge_InitialConditions(double[] pcond, SmallNumber[] gcond) {
        if (pcond.length != gcond.length)
            throw new RuntimeException(
                "Incorrect initialization: difference of size between "
                + "conditionsOnG and conditionsOnP");
        dimension = pcond.length;
        conditionsOnP = pcond;
        conditionsOnG = gcond;
    }

    /** Initializes the object with default values: zeros everywhere. */
    public p0ge_InitialConditions() {
        dimension = 1;
        conditionsOnP = new double[] {0};
        conditionsOnG = new SmallNumber[] {new SmallNumber(0.0)};
    }

    /** Returns <code>conditionsOnP</code>. */
    public double[] getConditionsOnP() {
        return this.conditionsOnP;
    }

    /** Returns <code>conditionsOnG</code>. */
    public SmallNumber[] getConditionsOnG() {
        return this.conditionsOnG;
    }

}
