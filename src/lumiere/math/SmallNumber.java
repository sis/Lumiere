package lumiere.math;

import beast.base.core.Description;

import java.text.DecimalFormat;


/**
 * This class contains the tools needed to represent and do basic
 * calculations with numbers in scientific representation.
 * <p>
 * For instance, 0.0523 would be written as 5.23E-2 in scientific
 * representation. In this implementation, the attribute "mantissa"
 * would be 5.23 and "exponent" would be -2.
 *
 * @author Jérémie Sciré (June 2016)
 */

@Description(
"This class contains the tools needed to represent and do basic "
+ "calculations with numbers in scientific representation. "
+ "For instance, 0.0523 would be written as 5.23E-2 in scientific "
+ "representation. In this implementation, the attribute 'mantissa' "
+ "would be 5.23 and 'exponent' would be -2.")

public class SmallNumber {

    private double mantissa;
    private int exponent;
    public static final int numbersPrinted = 12;
    public static final int threshold = -4;

    // number of orders of magnitude between two Small Numbers needed to
    // consider that the lower one is negligible compared to the higher one
    public static final int approximationThreshold = 53;

    final static int exponentMaxValueDouble = 1023;
    final static int exponentMinValueDouble = -1022;

    /** Initializes the object with zero. */
    public SmallNumber() {
        this.mantissa = 0;
        this.exponent = 0;
    }

    /** Initializes the object with given mantissa and exponent. */
    public SmallNumber(double r, int exp) {
        if (Double.isInfinite(r))
            throw new RuntimeException(
                "Unauthorized number (Infinity) used for conversion "
                + "into SmallNumber.");
        if (r == 0) {
            this.mantissa = 0;
            this.exponent = 0;
        }
        else {
            this.mantissa = r;
            this.exponent = exp;
        }
        this.update();
    }

    /** Initializes a SmallNumber from a double. */
    public SmallNumber(double num) {
        if (Double.isInfinite(num))
            throw new RuntimeException(
                "Unauthorized number (Infinity) used for conversion "
                + "into SmallNumber.");
        if (num == 0) {
            this.mantissa = 0;
            this.exponent = 0;
        }
        else {
            int numExponent = Math.getExponent(num);
            if(numExponent > 0) {
                // Instead of doing nothing in case of positive exponent,
                // we decide to scale it as well.
                this.mantissa = num * Math.pow(2, -numExponent);
                this.exponent = numExponent;
            }
            else {
                // Similar approach as in method
                // SmallNumberScaler.multiplyByPowerOfTwo().
                if(-numExponent>180)
                    this.mantissa = num * Math.pow(2, -numExponent);
                else {
                    int temporaryExponent = numExponent;
                    while(-temporaryExponent>30) {
                        num = num*(1<<30);
                        temporaryExponent+=30;
                    }
                    num = num * (1 << (-temporaryExponent));
                    this.mantissa = num;
                }
                this.exponent = numExponent;
            }
        }
    }

    /** ... */
    public void update() {
        if (Double.isInfinite(this.mantissa))
            throw new RuntimeException(
                "Unauthorized number (Infinity) used for conversion "
                + "in SmallNumber.");
        if (this.mantissa == 0)
            this.exponent = 0;
            // If the exponent is too low now, apply.
        else {
            int tempExp = Math.getExponent(this.mantissa);
            // Arbitrary threshold set at 200 to avoid underflow and overflow.
            if(Math.abs(tempExp) > 200){
                this.mantissa  *= Math.pow(2, -tempExp);
                this.exponent += tempExp;
            }
        }
    }

    /** Returns the <code>exponent</code>. */
    public int getExponent() {
        return this.exponent;
    }

    /** Returns the <code>mantissa</code>. */
    public double getMantissa() {
        return this.mantissa;
    }

    /**
     * Multiplies two small numbers.
     *
     * @return a new SmallNumber
     */
    public static SmallNumber multiply(SmallNumber a, SmallNumber b) {
        if (a.mantissa == 0 || b.mantissa == 0)
            return new SmallNumber(0);
        SmallNumber result =
            new SmallNumber(
                a.mantissa * b.mantissa,
                a.exponent + b.exponent
            );
        // Use update to make sure the result is in the correct representation.
        result.update();
        return result;
    }

    /**
     * Divides two small numbers.
     *
     * @return a new <code>SmallNumber</code>
     */
    public static SmallNumber divide(SmallNumber a, SmallNumber b) {
        if (a.mantissa == 0 || b.mantissa == 0)
            return new SmallNumber(0);
        SmallNumber result =
            new SmallNumber(
                a.mantissa / b.mantissa,
                a.exponent - b.exponent
            );
        // Use update to make sure the result is in the correct representation.
        result.update();
        return result;
    }

    /**
     * Multiplies a <code>SmallNumber</code> with a double.
     *
     * @return a new <code>SmallNumber</code>
     */
    public SmallNumber scalarMultiply(double lambda) {
        if (Double.isInfinite(lambda))
            throw new RuntimeException(
                "Unauthorized number (Infinity) used for multiplication "
                + "with a SmallNumber.");
        SmallNumber result =
            new SmallNumber(
                this.mantissa * lambda,
                this.exponent
            );
        result.update();
        return result;
    }

    /**
     * Increases the value of a <code>SmallNumber</code> by <code>exp</code>
     * orders of magnitude.
     */
    public void addExponent(int exp) {
        this.exponent += exp;
        this.update();
    }

    /**
     * Returns the negative of this small number.
     *
     * @return a new <code>SmallNumber</code>
     */
    public SmallNumber opposite() {
        return new SmallNumber(-this.mantissa, this.exponent);
    }

    /**
     * Adds two small numbers.
     *
     * @return a new <code>SmallNumber</code>
     */
    public static SmallNumber add(SmallNumber a, SmallNumber b){

        if (
            a.mantissa == 0
            || (
                (b.exponent - a.exponent) > approximationThreshold
                && b.mantissa !=0
            )
        )
            return b;
        else if (
            b.mantissa == 0 ||
            (a.exponent - b.exponent) > approximationThreshold
        )
            return a;
        else {
            SmallNumber c = new SmallNumber(0);
            if (a.exponent > b.exponent)
                c = new SmallNumber(
                    a.mantissa
                        + b.mantissa * Math.pow(2, b.exponent - a.exponent),
                    a.exponent
                );
            else
                c = new SmallNumber(
                    b.mantissa
                        + a.mantissa * Math.pow(2, a.exponent-b.exponent),
                    b.exponent
                );
            return c;
        }
    }

    /**
     * Returns this small number as a <code>double</code>, if possible,
     * <p>
     * WARNING: If the number stored in this <code>SmallNumber</code> is
     * too low to be accurately represented as a <code>double</code>,
     * revert will return 0.
     *
     * @return a <code>double</code>
     */
    public double revert() {
        if(this.exponent < exponentMinValueDouble)
            return 0;
        return this.mantissa * Math.pow(2, this.exponent);
    }

    /** ... */
    public static SmallNumber convertToScientificRepresentation(SmallNumber a) {

        if (a.mantissa == 0)
            return a;

        SmallNumber a10 = new SmallNumber(0);

        // Transformation of a * 2^(b) in alpha * c * 10^(beta+z)
        // where a = c * 10^z and 2^b = alpha * 10^beta
        double exponentBase10  = a.exponent * Math.log(2)/Math.log(10);

        int beta = (int) Math.floor(exponentBase10);
        double alpha = Math.pow(10, exponentBase10 - beta);

        double sign = Math.signum(a.mantissa);
        double log = Math.log10(Math.abs(a.mantissa));
        int z = (int)Math.floor(log);
        double c = sign * Math.pow(10, log - z);

        a10.mantissa = alpha * c;
        a10.exponent = beta+z;
        return a10;
    }

    /** Returns a string representation of the small number. */
    public String toString() {
        SmallNumber num10 = SmallNumber.convertToScientificRepresentation(this);
        String pattern = "0.";
        for (int i=1; i<numbersPrinted; i++)
            pattern += "#";
        DecimalFormat dF = new DecimalFormat(pattern);
        return "" + dF.format(num10.mantissa) + "E" + num10.exponent;
    }

    /** Converts an array of doubles into an array of small numbers. */
    public static SmallNumber[] getSmallNumbers(double[] numbers) {
        SmallNumber[] smallNums = new SmallNumber[numbers.length];
        for (int i=0; i<numbers.length; i++)
            smallNums[i] = new SmallNumber(numbers[i]);
        return smallNums;
    }

    /** Returns a string representation of an array of small numbers. */
    public static String toString(SmallNumber[] nums) {
        String result = "";
        for (SmallNumber sn: nums)
            result = result + sn.toString() + "\t";
        return result;
    }

    /**
     * Convert an array of small numbers into an array of doubles.
     * <p>
     * To be used when the risk of underflowing is negligible. If the small
     * number stored in one of the boxes is too low to be accurately
     * represented as a double, it will return as 0.
     */
    public static double[] getDoubles(SmallNumber[] nums) {
        double[] result = new double[nums.length];
        for (int i =0; i<nums.length; i++)
            result[i] = nums[i].revert();
        return result;
    }

    /** Returns the natural logarithm of this small number. */
    public double log() {
        if (this.mantissa <= 0)
            return Double.NEGATIVE_INFINITY;
        return Math.log(this.mantissa) + this.exponent*Math.log(2);
    }

    /** Returns whether this small number is finite. */
    public static boolean isInfinite(SmallNumber a) {
        return Double.isInfinite(a.mantissa);
    }

    /**
     * Returns the average exponent of an array of small numbers.
     * <p>
     * Can be useful for testing properties of the likelihood input and
     * results.
     */
    public static double averageExponent(SmallNumber[] nums) {
        double res = 0;
        int n = nums.length;
        for (int i=0; i<n; i++)
            res = res + (nums[i].exponent*1.0) / n;
        return res;
    }

    /**
     * Compares the minimal exponent of an array of small numbers with an
     * arbitrary threshold.
     *
     * @return 1 if the minimal exponent went over the threshold (in absolute
     *         value), 0 if it did not
     */
    public static int compareExponent(SmallNumber[] nums) {
        int min = nums[0].exponent;
        for (int i=1; i<nums.length; i++)
            min = min>nums[i].exponent? nums[i].exponent: min;
        int res = min < SmallNumber.threshold ? 1 : 0;
        return res;
    }

}
